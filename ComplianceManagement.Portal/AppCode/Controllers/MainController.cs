﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;



using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Nelibur.ObjectMapper;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Web.Security;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class MainController : Controller
    {
        public ActionResult SaveAllActivatedEvent(int ID, string Name, string Description, int EventInstanceID, string StartDate, int CustomerBranchID, string CustomerBranchName,
       string CustomerBranchStatus, int CustomerID, string Customer, int UserID, string UserName, string Email, string IsActive, string IsDeleted, int RoleID, string Role,
       int EventAssignmentID, int EventClassificationID, string NatureOfEventID, string ActivateDate)
        {
            string dt = DateTime.Now.ToString("dd-MM-yyyy");

            ActivateDate = ActivateDate.Substring(0, ActivateDate.IndexOf(" GMT"));
            DateTime ActivatedDate;

            DateTime.TryParseExact(ActivateDate, "ddd MMM d yyyy hh:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out ActivatedDate);

            // this has the output in your desired format.  Note that DateTime variables don't
            // have formats, dates only gain a format when you convert to a string so you can't
            // store a date in a DateTime variable as a given format
            string dateAsText = ActivatedDate.ToString("dd-MM-yyyy");
            string startdate = dt;
            try
            {
                Boolean chkNatureActiveDateflag = false;
               
                List<Tuple<long, long, int, DateTime, int>> EventScheDuleOnIDList = new List<Tuple<long, long, int, DateTime, int>>();

                List<SP_GetAllActivatedEvent_Result> obj = new List<SP_GetAllActivatedEvent_Result>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Type = 0;
                    if (EventClassificationID == 1)
                    {
                        Type = EventManagement.GetCompanyType(Convert.ToInt32(CustomerBranchID));

                    }
                    else
                    {
                        Type = 4;
                    }
                    if (!string.IsNullOrEmpty(NatureOfEventID))
                    {
                        Type = EventManagement.GetCompanyType(Convert.ToInt32(CustomerBranchID));

                        var ParentEvent = entities.SP_GetParentEventSelected(ID, Type).ToList();
                        List<long> EventList1 = new List<long>();
                        EventList1.Add(Convert.ToInt64(ID));
                        //save Event Nature
                        if (startdate != null)
                        {
                            DateTime Date = DateTime.ParseExact(startdate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            EventScheduleOn eventScheduleon = new EventScheduleOn();
                            eventScheduleon.EventInstanceID = EventInstanceID;
                            eventScheduleon.StartDate = Date;
                            eventScheduleon.EndDate = Date;
                            eventScheduleon.ClosedDate = Date;
                            eventScheduleon.HeldOn = Date;
                            eventScheduleon.Period = "0";
                            eventScheduleon.CreatedBy = UserID;
                            eventScheduleon.CreatedOn = DateTime.Now;
                            eventScheduleon.Description = NatureOfEventID; // txtDescription.Text;
                            eventScheduleon.IsDeleted = false;
                            entities.EventScheduleOns.Add(eventScheduleon);
                            entities.SaveChanges();

                            long eventAssignmentID = EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                            EventManagement.CreateEventReminders(Convert.ToInt64(ID), EventInstanceID, Convert.ToInt64(eventAssignmentID), UserID, Convert.ToDateTime(Date));
                            if (NatureOfEventID != "" && startdate != null)
                            {
                                chkNatureActiveDateflag = true;
                            }
                            if (chkNatureActiveDateflag == true)
                            {
                                foreach (var x in ParentEvent)
                                {
                                    int ParentEventID = Convert.ToInt32(x.ParentEventID);
                                    //Parent
                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                        IntermediateEventID = 0,
                                        SubEventID = 0,
                                        Date = Date,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(UserID),
                                        Type = Type,
                                    };
                                    EventManagement.CreateEventAssignDate(eventAssignDate);

                                    var SubEvent = entities.SP_GetEventData(ParentEventID, x.EventType, Type).ToList();

                                    foreach (var y in SubEvent)
                                    {
                                        if (y.IntermediateEventID == 0)
                                        {
                                            int SubEventID = Convert.ToInt32(y.SubEventID);
                                            EventAssignDate subeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = 0,
                                                SubEventID = SubEventID,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(UserID),
                                                Type = Type,
                                            };
                                            EventManagement.CreateEventAssignDate(subeventAssignDate);
                                        }
                                        else
                                        {
                                            int IntermediateEventID1 = Convert.ToInt32(y.SubEventID);
                                            EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = IntermediateEventID1,
                                                SubEventID = 0,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(UserID),
                                                Type = Type,
                                            };
                                            EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                            var Compliance = entities.SP_GetIntermediateComplianceAssignDays(ID, y.IntermediateEventID, IntermediateEventID1, Type).ToList();
                                            foreach (var z in Compliance)
                                            {
                                                int SubEventID = Convert.ToInt32(z.ComplianceID);
                                                EventAssignDate subeventAssignDate = new EventAssignDate()
                                                {
                                                    ParentEventID = ParentEventID,
                                                    EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                    IntermediateEventID = y.IntermediateEventID,
                                                    SubEventID = SubEventID,
                                                    Date = NextDate,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(UserID),
                                                    Type = Type,
                                                };
                                                EventManagement.CreateEventAssignDate(subeventAssignDate);
                                            }
                                        }
                                    }

                                    EventScheDuleOnIDList.Add(new Tuple<long, long, int, DateTime, int>(eventScheduleon.ID, ParentEventID, Type, ActivatedDate, Convert.ToInt32(CustomerBranchID)));

                                }
                            }

                        }
                    }
                    #region Set first Date to Activated Event
                    if (EventScheDuleOnIDList.Count > 0)
                    {
                        for (int i = 0; i < EventScheDuleOnIDList.Count; i++)
                        {
                            #region EventStructure : MainEvent -> SubEvent -> Compliance
                            var EventSchedueleData = EventManagement.GetActivatedEventData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                            int Type1 = Convert.ToInt32(EventScheDuleOnIDList[i].Item3);
                            int CusomerBranchID = Convert.ToInt32(EventScheDuleOnIDList[i].Item5);
                            int ParentEventID = EventSchedueleData.ParentEventID;
                            int SubEventID = EventSchedueleData.SubEventID;
                            int IntermediateEventID = EventSchedueleData.IntermediateEventID;
                            int EventScheduledOnID = Convert.ToInt32(EventScheDuleOnIDList[i].Item1);
                            DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                            long? EventClassificationID1 = EventManagement.GetEventClassification(ParentEventID);
                            if (IntermediateEventID == 0)
                            {
                                #region Sub Event
                                if (EventClassificationID1 == 1)
                                {
                                    var SubEventToComplianceList = EventManagement.GetSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);

                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = EventScheduledOnID,
                                        IntermediateEventID = 0,
                                        SubEventID = SubEventID,
                                        Date = ActiveDate,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(UserID),
                                        Type = Type,
                                    };
                                    Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                    for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                    {
                                        int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                                        int IsInternal = Convert.ToInt32(SubEventToComplianceList[j].IsInternal);

                                        if (IsInternal == 0) //Statutory
                                        {
                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);
                                                if (IsComplianceChecklistStatutory == true)
                                                {
                                                    //Change Generate flag Schedule change
                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                }
                                                string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                }
                                            }
                                        }
                                        else if (IsInternal == 1) //Internal
                                        {
                                            int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                            Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                            int days = Convert.ToInt32(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                            Boolean FlgCheck = false;
                                            FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                            if (FlgCheck == false)
                                            {
                                                EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                            }
                                        }
                                    }
                                }
                                else if (EventClassificationID1 == 2)
                                {
                                    var SubEventToComplianceList = EventManagement.GetNonSecretrialSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);

                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = EventScheduledOnID,
                                        IntermediateEventID = 0,
                                        SubEventID = SubEventID,
                                        Date = ActiveDate,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(UserID),
                                        Type = Type,
                                    };
                                    Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                    for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                    {
                                        int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                                        int IsInternal = Convert.ToInt32(SubEventToComplianceList[j].IsInternal);

                                        if (IsInternal == 0) //Statutory
                                        {
                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                if (IsComplianceChecklistStatutory == true)
                                                {
                                                    //Change Generate flag Schedule change
                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                }
                                                string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                }
                                            }
                                        }
                                        else if (IsInternal == 1) //Internal
                                        {
                                            int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                            int days = Convert.ToInt32(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                            Boolean FlgCheck = false;
                                            FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                            if (FlgCheck == false)
                                            {
                                                EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Intermediate Event
                                //DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                                var IntermediateSubEventSchedueleData = EventManagement.GetActivatedIntermediateSubEventSchedueleData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), IntermediateEventID, Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                int SubEventID1 = IntermediateSubEventSchedueleData.SubEventID;
                                if (EventClassificationID1 == 1)
                                {
                                    var SubEventToComplianceList = EventManagement.GetIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);
                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = EventScheduledOnID,
                                        IntermediateEventID = IntermediateEventID,
                                        SubEventID = SubEventID1,
                                        Date = ActiveDate,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(UserID),
                                        Type = Type,
                                    };
                                    Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                    for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                    {
                                        int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);
                                        int IsIntermediateInternal = Convert.ToInt32(SubEventToComplianceList[k].IsIntermediateInternal);

                                        if (IsIntermediateInternal == 0) //Statutory
                                        {
                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                if (IsComplianceChecklistStatutory == true)
                                                {
                                                    //Change Generate flag Schedule change
                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                }
                                                string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                }
                                            }
                                        }
                                        else if (IsIntermediateInternal == 1) //Internal
                                        {
                                            int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                            int days = Convert.ToInt32(SubEventToComplianceList[k].Days);
                                            Boolean FlgCheck = false;
                                            FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                            if (FlgCheck == false)
                                            {
                                                EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                            }
                                        }
                                    }
                                }
                                else if (EventClassificationID1 == 2)
                                {
                                    var SubEventToComplianceList = EventManagement.GetNonSecretrialIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);

                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = EventScheduledOnID,
                                        IntermediateEventID = IntermediateEventID,
                                        SubEventID = SubEventID1,
                                        Date = ActiveDate,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(UserID),
                                        Type = Type,
                                    };
                                    Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                    for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                    {
                                        int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);
                                        int IsIntermediateInternal = Convert.ToInt32(SubEventToComplianceList[k].IsIntermediateInternal);
                                        if (IsIntermediateInternal == 0) //Statutory
                                        {
                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                if (IsComplianceChecklistStatutory == true)
                                                {
                                                    //Change Generate flag Schedule change
                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                }
                                                string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                }
                                            }
                                        }
                                        else if (IsIntermediateInternal == 1) //Internal
                                        {
                                            int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                            int days = Convert.ToInt32(SubEventToComplianceList[k].Days);
                                            Boolean FlgCheck = false;
                                            FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                            if (FlgCheck == false)
                                            {
                                                EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                            }
                                        }
                                    }
                                }

                                #endregion
                            }

                            #endregion

                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
            }
            return new EmptyResult();
           
        }
        
        public class Thing
        {
            public Nullable<int> ID { get; set; }
            public long EventInstanceID { get; set; }
            public Nullable<int> CustomerBranchID { get; set; }
            public long UserID { get; set; }
            public Nullable<long> EventAssignmentID { get; set; }
            public Nullable<long> EventClassificationID { get; set; }
            public string NatureOfEventID { get; set; }
            public DateTime ActivateDate { get; set; }

        }

        public class Thing1
        {
            public Nullable<int> ID { get; set; }
            public Nullable<int> CustomerBranchID { get; set; }
            public string tbxStartDate { get; set; }
            public string ddlFilterPerformer { get; set; }
            public string ddlFilterReviewer { get; set; }
            public string ddlFilterApprover { get; set; }

        }
        public class Thing2
        {
            public List<long> EventID { get; set; }
        }

        public class NotAssignedComplianceClass
        {
            public long ID { get; set; }
            public long EventID { get; set; }
            public long CustomerBranchID { get; set; }
            public string EvnetName { get; set; }
            public string ComplianceName { get; set; }
            public string CustomerBranchName { get; set; }
        }

        public ActionResult SaveAllActivatedEventNew(List<Thing> ActivatedEvents)
        {

            try
            {
                List<Tuple<long, long, int, DateTime, int>> EventScheDuleOnIDList = new List<Tuple<long, long, int, DateTime, int>>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    foreach (var item in ActivatedEvents)
                    {
                        int Type = 0;
                        string dt = DateTime.Now.ToString("dd-MM-yyyy");

                        string startdate = dt;


                        if (item.EventClassificationID == 1)
                        {
                            Type = EventManagement.GetCompanyType(Convert.ToInt32(item.CustomerBranchID));

                        }
                        else
                        {
                            Type = 4;
                        }
                        if (!string.IsNullOrEmpty(item.NatureOfEventID))
                        {
                            Type = EventManagement.GetCompanyType(Convert.ToInt32(item.CustomerBranchID));

                            var ParentEvent = entities.SP_GetParentEventSelected(item.ID, Type).ToList();
                            List<long> EventList1 = new List<long>();
                            EventList1.Add(Convert.ToInt64(item.ID));
                            //save Event Nature
                            if (startdate != null)
                            {
                                DateTime Date = DateTime.ParseExact(startdate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                EventScheduleOn eventScheduleon = new EventScheduleOn();
                                eventScheduleon.EventInstanceID = item.EventInstanceID;
                                eventScheduleon.StartDate = Date;
                                eventScheduleon.EndDate = Date;
                                eventScheduleon.ClosedDate = Date;
                                eventScheduleon.HeldOn = Date;
                                eventScheduleon.Period = "0";
                                eventScheduleon.CreatedBy = item.UserID;
                                eventScheduleon.CreatedOn = DateTime.Now;
                                eventScheduleon.Description = item.NatureOfEventID; // txtDescription.Text;
                                eventScheduleon.IsDeleted = false;
                                entities.EventScheduleOns.Add(eventScheduleon);
                                entities.SaveChanges();

                                long eventAssignmentID = EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                                EventManagement.CreateEventReminders(Convert.ToInt64(item.ID), item.EventInstanceID, Convert.ToInt64(eventAssignmentID), item.UserID, Convert.ToDateTime(Date));

                                foreach (var x in ParentEvent)
                                {
                                    int ParentEventID = Convert.ToInt32(x.ParentEventID);
                                    //Parent
                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                        IntermediateEventID = 0,
                                        SubEventID = 0,
                                        Date = Date,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(item.UserID),
                                        Type = Type,
                                    };
                                    EventManagement.CreateEventAssignDate(eventAssignDate);

                                    var SubEvent = entities.SP_GetEventData(ParentEventID, x.EventType, Type).ToList();

                                    foreach (var y in SubEvent)
                                    {
                                        if (y.IntermediateEventID == 0)
                                        {
                                            int SubEventID = Convert.ToInt32(y.SubEventID);
                                            EventAssignDate subeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = 0,
                                                SubEventID = SubEventID,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(item.UserID),
                                                Type = Type,
                                            };
                                            EventManagement.CreateEventAssignDate(subeventAssignDate);
                                        }
                                        else
                                        {
                                            int IntermediateEventID1 = Convert.ToInt32(y.SubEventID);
                                            EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = IntermediateEventID1,
                                                SubEventID = 0,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(item.UserID),
                                                Type = Type,
                                            };
                                            EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                            var Compliance = entities.SP_GetIntermediateComplianceAssignDays(item.ID, y.IntermediateEventID, IntermediateEventID1, Type).ToList();
                                            foreach (var z in Compliance)
                                            {
                                                int SubEventID = Convert.ToInt32(z.ComplianceID);
                                                EventAssignDate subeventAssignDate = new EventAssignDate()
                                                {
                                                    ParentEventID = ParentEventID,
                                                    EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                    IntermediateEventID = y.IntermediateEventID,
                                                    SubEventID = SubEventID,
                                                    Date = NextDate,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(item.UserID),
                                                    Type = Type,
                                                };
                                                EventManagement.CreateEventAssignDate(subeventAssignDate);
                                            }
                                        }
                                    }

                                    EventScheDuleOnIDList.Add(new Tuple<long, long, int, DateTime, int>(eventScheduleon.ID, ParentEventID, Type, item.ActivateDate, Convert.ToInt32(item.CustomerBranchID)));

                                }
                            }

                        }


                        #region Set first Date to Activated Event
                        if (EventScheDuleOnIDList.Count > 0)
                        {
                            for (int i = 0; i < EventScheDuleOnIDList.Count; i++)
                            {
                                #region EventStructure : MainEvent -> SubEvent -> Compliance
                                var EventSchedueleData = EventManagement.GetActivatedEventData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                int Type1 = Convert.ToInt32(EventScheDuleOnIDList[i].Item3);
                                int CusomerBranchID = Convert.ToInt32(EventScheDuleOnIDList[i].Item5);
                                int ParentEventID = EventSchedueleData.ParentEventID;
                                int SubEventID = EventSchedueleData.SubEventID;
                                int IntermediateEventID = EventSchedueleData.IntermediateEventID;
                                int EventScheduledOnID = Convert.ToInt32(EventScheDuleOnIDList[i].Item1);
                                DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                                long? EventClassificationID1 = EventManagement.GetEventClassification(ParentEventID);
                                if (IntermediateEventID == 0)
                                {
                                    #region Sub Event
                                    if (EventClassificationID1 == 1)
                                    {
                                        var SubEventToComplianceList = EventManagement.GetSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);

                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = EventScheduledOnID,
                                            IntermediateEventID = 0,
                                            SubEventID = SubEventID,
                                            Date = ActiveDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(item.UserID),
                                            Type = Type,
                                        };
                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                        for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                        {
                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                                            int IsInternal = Convert.ToInt32(SubEventToComplianceList[j].IsInternal);

                                            if (IsInternal == 0) //Statutory
                                            {
                                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                {
                                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);
                                                    if (IsComplianceChecklistStatutory == true)
                                                    {
                                                        //Change Generate flag Schedule change
                                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                    }
                                                    string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                    Boolean FlgCheck = false;
                                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                    if (FlgCheck == false)
                                                    {
                                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                    }
                                                }
                                            }
                                            else if (IsInternal == 1) //Internal
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                int days = Convert.ToInt32(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                }
                                            }
                                        }
                                    }
                                    else if (EventClassificationID1 == 2)
                                    {
                                        var SubEventToComplianceList = EventManagement.GetNonSecretrialSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);

                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = EventScheduledOnID,
                                            IntermediateEventID = 0,
                                            SubEventID = SubEventID,
                                            Date = ActiveDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(item.UserID),
                                            Type = Type,
                                        };
                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                        for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                        {
                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                                            int IsInternal = Convert.ToInt32(SubEventToComplianceList[j].IsInternal);

                                            if (IsInternal == 0) //Statutory
                                            {
                                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                {
                                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                    if (IsComplianceChecklistStatutory == true)
                                                    {
                                                        //Change Generate flag Schedule change
                                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                    }
                                                    string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                    Boolean FlgCheck = false;
                                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                    if (FlgCheck == false)
                                                    {
                                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                    }
                                                }
                                            }
                                            else if (IsInternal == 1) //Internal
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                int days = Convert.ToInt32(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Intermediate Event
                                    //DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                                    var IntermediateSubEventSchedueleData = EventManagement.GetActivatedIntermediateSubEventSchedueleData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), IntermediateEventID, Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                    int SubEventID1 = IntermediateSubEventSchedueleData.SubEventID;
                                    if (EventClassificationID1 == 1)
                                    {
                                        var SubEventToComplianceList = EventManagement.GetIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);
                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = EventScheduledOnID,
                                            IntermediateEventID = IntermediateEventID,
                                            SubEventID = SubEventID1,
                                            Date = ActiveDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(item.UserID),
                                            Type = Type,
                                        };
                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                        for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                        {
                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);
                                            int IsIntermediateInternal = Convert.ToInt32(SubEventToComplianceList[k].IsIntermediateInternal);

                                            if (IsIntermediateInternal == 0) //Statutory
                                            {
                                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                {
                                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                    if (IsComplianceChecklistStatutory == true)
                                                    {
                                                        //Change Generate flag Schedule change
                                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                    }
                                                    string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                    Boolean FlgCheck = false;
                                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                    if (FlgCheck == false)
                                                    {
                                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                    }
                                                }
                                            }
                                            else if (IsIntermediateInternal == 1) //Internal
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                int days = Convert.ToInt32(SubEventToComplianceList[k].Days);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                }
                                            }
                                        }
                                    }
                                    else if (EventClassificationID1 == 2)
                                    {
                                        var SubEventToComplianceList = EventManagement.GetNonSecretrialIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);

                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = EventScheduledOnID,
                                            IntermediateEventID = IntermediateEventID,
                                            SubEventID = SubEventID1,
                                            Date = ActiveDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(item.UserID),
                                            Type = Type,
                                        };
                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                        for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                        {
                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);
                                            int IsIntermediateInternal = Convert.ToInt32(SubEventToComplianceList[k].IsIntermediateInternal);
                                            if (IsIntermediateInternal == 0) //Statutory
                                            {
                                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                {
                                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                    if (IsComplianceChecklistStatutory == true)
                                                    {
                                                        //Change Generate flag Schedule change
                                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                    }
                                                    string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                    Boolean FlgCheck = false;
                                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                    if (FlgCheck == false)
                                                    {
                                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                    }
                                                }
                                            }
                                            else if (IsIntermediateInternal == 1) //Internal
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                int days = Convert.ToInt32(SubEventToComplianceList[k].Days);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                }
                                            }
                                        }
                                    }

                                    #endregion
                                }

                                #endregion

                            }
                        }
                        #endregion
                    }
                }
          
            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = "Select and provide details of atleast one event." });
            }
            return Json(new { status = "success", message = "Successfully Activated" });
        }

        public ActionResult CheckAllNotAssignedComplinceListForEvent(List<Thing> ActivatedEvents)
        {

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<NotAssignedComplianceClass> NotAssigned = new List<NotAssignedComplianceClass>();
                    List<long> EventList = new List<long>();
                    foreach (var item in ActivatedEvents)
                    {   
                        string dt = DateTime.Now.ToString("dd-MM-yyyy");
                        string startdate = dt;

                        string eventnature = item.NatureOfEventID;

                        if (!string.IsNullOrEmpty(eventnature))
                        {
                            int EventClassificationID = Convert.ToInt32(item.EventClassificationID);

                            long branch = Convert.ToInt64(item.CustomerBranchID);

                            long eventId = Convert.ToInt64(item.ID);

                            if (EventClassificationID == 1) //Secretrial 
                            {
                                var exceptComplianceIDsList = EventManagement.CheckAllEventCompliance(Convert.ToInt32(eventId), Convert.ToInt32(branch));
                                Session["exceptComplianceIDs"] = exceptComplianceIDsList;

                                EventList.Add(Convert.ToInt64(eventId));
                                int CustomerBranchId = Convert.ToInt32(branch);

                                if (exceptComplianceIDsList.Count > 0)
                                {
                                    var data = EventManagement.GetAllNotAssignedComplinceListForEvent(1, exceptComplianceIDsList, EventList, CustomerBranchId, -1);

                                    foreach (var item1 in data)
                                    {

                                        NotAssigned.Add(new NotAssignedComplianceClass
                                        {
                                            ID = item1.ID,
                                            EventID = item1.EventID,
                                            EvnetName = item1.EvnetName,
                                            ComplianceName = item1.ComplianceName,
                                            CustomerBranchName = item1.CustomerBranchName,
                                            CustomerBranchID = item1.CustomerBranchID,
                                        });
                                    }

                                }
                            }

                            else if (EventClassificationID == 2) //NonSecretrial
                            {
                                var exceptComplianceIDsList = EventManagement.CheckAllEventComplianceNonSecretrial(Convert.ToInt32(eventId), Convert.ToInt32(branch));
                                Session["exceptComplianceIDs"] = exceptComplianceIDsList;
                                EventList.Add(Convert.ToInt32(eventId));
                                int CustomerBranchId = Convert.ToInt32(branch);

                                if (exceptComplianceIDsList.Count > 0)
                                {
                                    var data = EventManagement.GetAllNotAssignedComplinceListForEvent(1, exceptComplianceIDsList, EventList, CustomerBranchId, -1);
                                    foreach (var item1 in data)
                                    {

                                        NotAssigned.Add(new NotAssignedComplianceClass
                                        {
                                            ID = item1.ID,
                                            EventID = item1.EventID,
                                            EvnetName = item1.EvnetName,
                                            ComplianceName = item1.ComplianceName,
                                            CustomerBranchName = item1.CustomerBranchName,
                                            CustomerBranchID = item1.CustomerBranchID,
                                        });
                                    }
                                }
                            }

                        }

                    }

                    if (NotAssigned.Count > 0)
                    {
                        return Json(new { status = NotAssigned,EventID= EventList, message = "NotComplianceAssigned" });
                    }

                }

            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = "Please select atleast one event" });
            }
            return Json(new { status = "success", message = "No NotComplianceAssigned" });
        }

        public ActionResult GetCheckdEvent(string EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
               
                List<long> EventList = EventID.Split(',').Select(long.Parse).ToList();
               
                  var eventList = (from row in entities.Events
                                 where EventList.Contains(row.ID) && row.IsDeleted == false
                                 select new { row.ID,row.Name }).ToList();

                return Json(new { status = eventList, message = "No NotComplianceAssigned" });
            }
        }
        public ActionResult SaveComplianceList(List<Thing1> ActivatedEvents)
        {

            try
            {
                List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

                foreach (var item in ActivatedEvents)
                {
                    string ComplianceID =Convert.ToString(item.ID);
                    ComplianceInstance instance = new ComplianceInstance();
                    instance.ComplianceId = Convert.ToInt64(ComplianceID);
                    instance.CustomerBranchID = Convert.ToInt32(item.CustomerBranchID);
                    instance.ScheduledOn = DateTime.ParseExact(item.tbxStartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    if (item.ddlFilterPerformer != null)
                    {
                        ComplianceAssignment assignment = new ComplianceAssignment();
                        assignment.UserID = Convert.ToInt32(item.ddlFilterPerformer);
                        assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
                    }
                    if (item.ddlFilterReviewer != null)
                    {

                        ComplianceAssignment assignment1 = new ComplianceAssignment();
                        assignment1.UserID = Convert.ToInt32(item.ddlFilterReviewer);
                        assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
                    }

                    if (item.ddlFilterApprover != null)
                    {
                        ComplianceAssignment assignment2 = new ComplianceAssignment();
                        assignment2.UserID = Convert.ToInt32(item.ddlFilterApprover);
                        assignment2.RoleID = RoleManagement.GetByCode("APPR").ID;
                        assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment2));
                    }
                }
                
                if (assignments.Count != 0)
                {
                    Business.ComplianceManagement.CreateInstancesEventBased(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User);
                    return Json(new { status = "success", message = "ComplianceList Saved" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = "Please select atleast one event" });
            }
            return Json(new { status = "success", message = "ComplianceList Not Saved" });
        }



        // GET: Main
        public RedirectResult Index()
        {
            return Redirect("~/Login.aspx");
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult Create()
        {

            return View();
        }

        [HttpGet]
        public ActionResult ComplianceNewActs()
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                int user = AuthenticationHelper.UserID;
                long customer = AuthenticationHelper.CustomerID;
                ComplianceActs acts = new ComplianceActs();

                List<ComplianceCategory> ComplianceCategory = null;

                List<ComplianceType> ComplianceTypeModelList = null;

                try
                {
                    ComplianceCategory = ComplianceCategoryManagement.GetAll();
                    if (ComplianceCategory != null && ComplianceCategory.Count > 0)
                    {
                        TinyMapper.Bind<List<ComplianceCategory>, ComplianceCategoryModels>();
                        acts.category.CategoriesList = TinyMapper.Map<List<ComplianceCategoryModels>>(ComplianceCategory);
                    }

                    ComplianceTypeModelList = ComplianceTypeManagement.GetAll();
                    if (ComplianceTypeModelList != null && ComplianceTypeModelList.Count > 0)
                    {
                        acts.ComplianceTypes.ComplianceTypeList.AddRange(ComplianceTypeModelList);
                    }


                }
                catch (Exception ex)
                {

                }
                return View(acts);
            }
            else
            {
               
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
              //  FormsAuthentication.RedirectToLoginPage();
            }
        }


        [HttpPost]
        public ActionResult ComplianceNewActs(ComplianceActs ComplianceActs ,string ActMappingModelList_ActMapping_AM_ActID)
        {
            int user = AuthenticationHelper.UserID;
            long customer = AuthenticationHelper.CustomerID;
            //ComplianceActs acts = new ComplianceActs();

            List<ComplianceCategory> ComplianceCategory = null;

            List<ComplianceType> ComplianceTypeModelList = null;
            string AM_ACTID = ActMappingModelList_ActMapping_AM_ActID;
            string ACTID = ComplianceActs.ActMappingModelList.HiddenAct_ID;
            try
            {
                ComplianceCategory = ComplianceCategoryManagement.GetAll();
                if (ComplianceCategory != null && ComplianceCategory.Count > 0)
                {
                    TinyMapper.Bind<List<ComplianceCategory>, ComplianceCategoryModels>();
                    ComplianceActs.category.CategoriesList = TinyMapper.Map<List<ComplianceCategoryModels>>(ComplianceCategory);
                }

                ComplianceTypeModelList = ComplianceTypeManagement.GetAll();
                if (ComplianceTypeModelList != null && ComplianceTypeModelList.Count > 0)
                {
                    ComplianceActs.ComplianceTypes.ComplianceTypeList.AddRange(ComplianceTypeModelList);
                }
                if (ModelState.IsValid)
                {

                    if(ActManagement.UpdateAllActMapping(AM_ACTID, ACTID))
                    {
                        ComplianceActs.Message = true;
                    }
                }
               

            }
            catch (Exception ex)
            {

            }
            return View(ComplianceActs);
        }

        public ActionResult AvacomComplianceMapping()
        {
            SectionMappingModel acts = new SectionMappingModel();

            List<ComplianceCategory> ComplianceCategory = null;
            List<RLCS_Act_Mapping> RLCSActMappingList = null;
            List<ComplianceType> ComplianceTypeModelList = null;
       
            try
            {
                ViewBag.Lst = Enumerations.GetAll<Frequency>();
                ComplianceCategory = ComplianceCategoryManagement.GetAll();
                if (ComplianceCategory != null && ComplianceCategory.Count > 0)
                {
                    TinyMapper.Bind<List<ComplianceCategory>, ComplianceCategoryModels>();
                    acts.category.CategoriesList = TinyMapper.Map<List<ComplianceCategoryModels>>(ComplianceCategory);
                }

                ComplianceTypeModelList = ComplianceTypeManagement.GetAll();
                if (ComplianceTypeModelList != null && ComplianceTypeModelList.Count > 0)
                {
                    acts.ComplianceTypes.ComplianceTypeList.AddRange(ComplianceTypeModelList);
                }
                RLCSActMappingList = ActManagement.GetAllActsMapping("Mapped");
                if (RLCSActMappingList.Count > 0 && RLCSActMappingList != null)
                {
                    TinyMapper.Bind<RLCS_Act_Mapping, ActMappingModel>();
                    acts.ActMappingModelList.RLCS_Act_MappingList = TinyMapper.Map<List<ActMappingModel>>(RLCSActMappingList);

                }
               

            }
            catch (Exception ex)
            {

            }
            return View(acts);
            
        }

        [HttpPost]
        public ActionResult AvacomComplianceMapping(SectionMappingModel acts, string SM_SectionID)
        {
            //ComplianceActs acts = new ComplianceActs();

            List<ComplianceCategory> ComplianceCategory = null;
            List<RLCS_Act_Mapping> RLCSActMappingList = null;
            List<ComplianceType> ComplianceTypeModelList = null;
            string AM_ACTID = SM_SectionID;
            string ComplianID = acts.HiddenAct_ID;
            try
            {
                ViewBag.Lst = Enumerations.GetAll<Frequency>();
                ComplianceCategory = ComplianceCategoryManagement.GetAll();
                if (ComplianceCategory != null && ComplianceCategory.Count > 0)
                {
                    TinyMapper.Bind<List<ComplianceCategory>, ComplianceCategoryModels>();
                    acts.category.CategoriesList = TinyMapper.Map<List<ComplianceCategoryModels>>(ComplianceCategory);
                }

                ComplianceTypeModelList = ComplianceTypeManagement.GetAll();
                if (ComplianceTypeModelList != null && ComplianceTypeModelList.Count > 0)
                {
                    acts.ComplianceTypes.ComplianceTypeList.AddRange(ComplianceTypeModelList);
                }
                RLCSActMappingList = ActManagement.GetAllActsMapping("Mapped");
                if (RLCSActMappingList.Count > 0 && RLCSActMappingList != null)
                {
                    TinyMapper.Bind<RLCS_Act_Mapping, ActMappingModel>();
                    acts.ActMappingModelList.RLCS_Act_MappingList = TinyMapper.Map<List<ActMappingModel>>(RLCSActMappingList);

                }
                if (ModelState.ContainsKey("ActMappingModelList.HiddenAct_ID"))
                    ModelState["ActMappingModelList.HiddenAct_ID"].Errors.Clear();
                if (ModelState.ContainsKey("ActMappingModelList.AVACOM_ActID"))
                    ModelState["ActMappingModelList.AVACOM_ActID"].Errors.Clear();
                if (ModelState.IsValid)
                {

                    if (ActManagement.UpdateAllSectionMapping(ComplianID, AM_ACTID))
                    {
                        acts.Message = true;
                    }
                }


            }
            catch (Exception ex)
            {

            }
            return View(acts);

        }

        [HttpPost]
        public ActionResult LoadData(string search, string compliancetype, string categorytype, string RLCS_ACT_ID, string frequency, string val)
        {
            SectionMappingModel acts = new SectionMappingModel();
            List<ComplianceView> ComplianceViewList = null;
            try
            {
                acts.draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                //Get Sort columns value
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int cmtype = 0;
                if (val == null)
                {
                    cmtype = -1;

                }
                ComplianceViewList = ComplianceManagement.Business.ComplianceManagement.GetAllNew(Convert.ToInt32(compliancetype), Convert.ToInt32(categorytype), Convert.ToInt32(frequency), cmtype, Convert.ToInt32(RLCS_ACT_ID), search);
                if (ComplianceViewList.Count > 0)
                {
                 acts.ComplianceModel.ComplianceList = ComplianceViewList.Skip(skip).Take(pageSize).ToList();
                    TinyMapper.Bind<List<ComplianceView>, ComplianceModel>();
                    acts.ComplianceModel.ComplianceListNew = TinyMapper.Map<List<ComplianceModel>>(ComplianceViewList);

                }
                acts.recordsTotal = ComplianceViewList.Count;
                acts.recordsFiltered = acts.recordsTotal;
                acts.ComplianceModel.ComplianceListNew = acts.ComplianceModel.ComplianceListNew.Skip(skip).Take(pageSize).ToList();
                
                acts.ComplianceModel.ComplianceListNew.ForEach(x =>
                {
                    if (x.Frequency == 0)
                        x.FrquencyName = "MONTHLY";
                    if (x.Frequency == 1)
                        x.FrquencyName = "Quarterly";
                    if (x.Frequency == 2)
                        x.FrquencyName = "HalfYearly";
                    if (x.Frequency == 3)
                        x.FrquencyName = "Annual";
                    if (x.Frequency == 4)
                        x.FrquencyName = "FourMonthly";
                    if (x.Frequency == 5)
                        x.FrquencyName = "TwoYearly";
                    if (x.Frequency == 6)
                        x.FrquencyName = "SevenYearly";
                    if (x.Frequency == 7)
                        x.FrquencyName = "Daily";
                    if (x.Frequency == 8)
                        x.FrquencyName = "Weekly";
                    if (x.RiskType == 0)
                        x.Risk = "High";
                    if (x.RiskType == 1)
                        x.Risk = "Medium";
                    if (x.RiskType == 2)
                        x.Risk = "Low";
                    if (x.UploadDocument == true)
                        x.UploadName = "Yes";
                    if (x.UploadDocument == false)
                        x.UploadName = "No";
                   

                });
            }

            catch (Exception Ex)
            {

            }
            var res = new
            {
                iTotalRecords = acts.recordsTotal,//records per page 
                iTotalDisplayRecords = acts.recordsTotal, //total table count
               aaData = acts.ComplianceModel.ComplianceListNew,

            };

            //
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult GetAllCompliances()
        //{
        //    ComplianceActs acts = new ComplianceActs();
        //    List<ComplianceView> ComplianceViewList = null;
        //    JsonResult result = null;
        //    try
        //    {

            //        //if (!string.IsNullOrEmpty(compliancetype) )
            //        //{
            //        //|| !string.IsNullOrEmpty(categorytype)    ComplianceViewList = ComplianceManagement.Business.ComplianceManagement.GetAll1(Convert.ToInt32(catetype), Convert.ToInt32(compliancetype), Convert.ToInt32(fre), Convert.ToInt32(CmType),null);
            //        //

            //        ComplianceViewList = ComplianceManagement.Business.ComplianceManagement.GetAll();
            //            if (ComplianceViewList.Count > 0 && ComplianceViewList != null)
            //            {
            //                acts.ComplianceModel.ComplianceList.AddRange(ComplianceViewList);


            //            }
            //        int totalRecords = acts.ComplianceModel.ComplianceList.Count;
            //        //}
            //        //else
            //        //{
            //        //ComplianceViewList = ComplianceManagement.Business.ComplianceManagement.GetAll();
            //        //if (ComplianceViewList.Count > 0 && ComplianceViewList != null)
            //        //{
            //        //    acts.ComplianceModel.ComplianceList.AddRange(ComplianceViewList);

            //        //    //result.MaxJsonLength = 8675309;
            //        //}
            //        //}


            //    }
            //    catch (Exception Ex)
            //    {

            //    }
            //    return Json(acts.ComplianceModel.ComplianceList.Take(1000), JsonRequestBehavior.AllowGet);
            //}
        public ActionResult GetActMapping(string val)
        {
            ActMappingModel ActMappingModel = new ActMappingModel();
            List<RLCS_Act_Mapping> RLCSActMappingList = null;
            try
            {

                RLCSActMappingList = ActManagement.GetAllActsMapping(val);
                if (RLCSActMappingList.Count > 0 && RLCSActMappingList != null)
                {
                   // ActMappingModel.RLCS_Act_MappingList.AddRange(RLCSActMappingList);
                    TinyMapper.Bind<RLCS_Act_Mapping, ActMappingModel>();
                    ActMappingModel.RLCS_Act_MappingList= TinyMapper.Map<List<ActMappingModel>>(RLCSActMappingList);
                  
                }
            }
            catch(Exception Ex)
            {

            }
            return Json(ActMappingModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllActsMapping(string catetype,string compliancetype)
        {
            ComplianceActs acts = new ComplianceActs();
            List<ActView> ActsViewList = null;
            try
            {

                ActsViewList = ActManagement.GetAllActs(Convert.ToInt32(compliancetype), Convert.ToInt32(compliancetype), null);
                if (ActsViewList != null && ActsViewList.Count > 0)
                {
                    acts.Acts.ActList.AddRange(ActsViewList);
                }
            }
            catch (Exception Ex)
            {

            }
            return Json(acts.Acts.ActList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllSections()
        {
            SectionMappingModel acts = new SectionMappingModel();
            List<RLCS_Section_Compliance_Mapping> SectionMappingModelList = null;
            try
            {

                SectionMappingModelList = ActManagement.GetALLSectionMapping();
                if (SectionMappingModelList != null && SectionMappingModelList.Count > 0)
                {
                    TinyMapper.Bind<SectionMappingModel, RLCS_Section_Compliance_Mapping>();
                    acts.SectionList = TinyMapper.Map<List<SectionMappingModel>>(SectionMappingModelList);
                  
                }
            }
            catch (Exception Ex)
            {

            }
            return Json(acts.SectionList, JsonRequestBehavior.AllowGet);
        }
    }
}