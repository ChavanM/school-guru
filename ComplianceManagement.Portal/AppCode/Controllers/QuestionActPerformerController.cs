﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class QuestionActPerformerController : Controller
    {
        // GET: QuestionActPerformer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult QuestionActPerformerQuestions()
        {
            return View();
        }


        [HttpGet]
        public ActionResult GetActAgrup()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;

                var role = (from A in entities.IMA_GetAssignedAct(AuthenticationHelper.UserID, "ACTGROUP", "PER")
                            select new
                            {
                                ID = A.ID,
                                Name = A.Name,
                                ActGroupID = A.ActGroupID
                            }).ToList();

                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        #region  Un Taggged Question  Act Mapping    

        [HttpGet]
        public ActionResult GetBusinessActivity()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.BusinessActivityTypes.Where(x => x.IsActive == false).Select(model => new { model.ID, model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetActApplicability()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.Act_Applicability.Select(model => new { model.ID, model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetLocationType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.LocationTypes.Select(model => new { model.ID, model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetCompanyType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.CompanyTypes.Select(model => new { model.ID, model.Name }).ToList();
                //var role = entities.CompanyTypes.Select(model => new { id=model.ID, label=model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetPossibleQuestionAnswer()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.PossibleQuestionAnswers.Select(model => new { QuestionId = model.QuestionId, ID = model.ID, Name = model.Answer }).ToList();
                //var role = entities.CompanyTypes.Select(model => new { id=model.ID, label=model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getAllActs(int actgroupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.sp_GetActQuestiondetails(actgroupid,AuthenticationHelper.UserID)
                                    select usr).ToList();

               
                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public string AddQuestionActMapping(QA_Mapping detail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        Questions_ActMapping Qmst = new Questions_ActMapping();
                        Qmst.ActId = detail.ActID;
                        Qmst.QuestionID = detail.QuestionID;
                        Qmst.ControllId = detail.ControlId;
                        Qmst.AnswerTOQ = detail.ServiceID;
                        Qmst.AnswerValue = detail.AnswerValue;
                        if (detail.ControlId == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.AnswerSingle;
                        }
                        else if (detail.ControlId == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.AnswerMultiple;
                        }
                        Qmst.StatusId = 2;
                        Qmst.AnsweRangeMin = detail.min;
                        Qmst.AnsweRangeMax = detail.max;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        Qmst.LocationTypeID = detail.LocationTypeID;
                        Act_CompanyTypeMapping ACTM = new Act_CompanyTypeMapping()
                        {
                            ActID = detail.ActID,
                            TypeID = (int)detail.CompanyTypeID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };

                        if (!Act_CompanyTypeMappingExists(ACTM))
                        {
                            entities.Act_CompanyTypeMapping.Add(ACTM);
                            entities.SaveChanges();
                        }

                        Act_BusinessActivityMapping ABAM = new Act_BusinessActivityMapping()
                        {
                            ActID = detail.ActID,
                            BATypeID = (int)detail.BusinessActivityID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };
                        if (!ActBusinessActivityMappingExists(ABAM))
                        {
                            entities.Act_BusinessActivityMapping.Add(ABAM);
                            entities.SaveChanges();
                        }
                        Act_LoctionTypeMapping ALTM = new Act_LoctionTypeMapping()
                        {
                            ActID = detail.ActID,
                            LoctionTypeID = (int)detail.LocationTypeID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };

                        if (!Act_LoctionTypeMappingExists(ALTM))
                        {
                            entities.Act_LoctionTypeMapping.Add(ALTM);
                            entities.SaveChanges();
                        }

                        Act_ActApplicabilityMapping AAAM = new Act_ActApplicabilityMapping()
                        {
                            ActID = detail.ActID,
                            Act_ApplicabilityID = (int)detail.ActApplicabilityID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };
                        if (!ActApplicabilityMappingExists(AAAM))
                        {
                            entities.Act_ActApplicabilityMapping.Add(AAAM);
                            entities.SaveChanges();
                        }
                        var query = (from row in entities.Acts
                                     where row.ID == detail.ActID
                                     select row).FirstOrDefault();
                        if (query !=null)
                        {
                            query.LocationTypeID = detail.LocationTypeID;
                            entities.SaveChanges();
                        }
                        if (Questions_ActMappingExists(Qmst))
                        {
                            //result = "Question name already exists";
                            result = "Details already exists!";
                        }
                        else
                        {
                            entities.Questions_ActMapping.Add(Qmst);
                            entities.SaveChanges();
                            var QuestionID = Qmst.Id;
                            result = "Details Saved Successfully!";
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }

        public bool ActApplicabilityMappingExists(Act_ActApplicabilityMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_ActApplicabilityMapping
                             where row.IsActive == false
                                  && row.ActID == customer.ActID
                                  && row.Act_ApplicabilityID == customer.Act_ApplicabilityID
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public bool ActBusinessActivityMappingExists(Act_BusinessActivityMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_BusinessActivityMapping
                             where row.IsActive == false
                                  && row.ActID == customer.ActID
                                  && row.BATypeID == customer.BATypeID
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public bool Act_CompanyTypeMappingExists(Act_CompanyTypeMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_CompanyTypeMapping
                             where row.IsActive == false
                                  && row.ActID == customer.ActID
                                  && row.TypeID == customer.TypeID                                  
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public bool Questions_ActMappingExists(Questions_ActMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Questions_ActMapping
                             where row.IsActive == false
                                  && row.ActId == customer.ActId
                                  && row.QuestionID == customer.QuestionID
                                  && row.ControllId == customer.ControllId
                             select row);

                if (customer.Id > 0)
                {
                    query = query.Where(entry => entry.Id != customer.Id);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }


        public bool Act_LoctionTypeMappingExists(Act_LoctionTypeMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_LoctionTypeMapping
                             where row.IsActive == false
                                  && row.ActID == customer.ActID
                                  && row.LoctionTypeID == customer.LoctionTypeID
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        [HttpPost]
        public ActionResult SaveActs(QAMappingEntry TimeSheets)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (QA_Mapping detail in TimeSheets.Approvals)
                    {
                        int empId = Convert.ToInt32(Session["userID"]);
                        Questions_ActMapping Qmst = new Questions_ActMapping();
                        Qmst.ActId = detail.ActID;
                        Qmst.QuestionID = detail.QuestionID;
                        Qmst.ControllId = detail.ControlId;
                        Qmst.AnswerTOQ = detail.ServiceID;
                        Qmst.AnswerValue = detail.AnswerValue;
                        if (detail.ControlId == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.AnswerSingle;
                        }
                        else if (detail.ControlId == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.AnswerMultiple;
                        }
                        Qmst.StatusId = 2;
                        Qmst.AnsweRangeMin = detail.min;
                        Qmst.AnsweRangeMax = detail.max;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        Qmst.LocationTypeID = detail.LocationTypeID;
                        entities.Questions_ActMapping.Add(Qmst);
                        entities.SaveChanges();



                        Act_CompanyTypeMapping ACTM = new Act_CompanyTypeMapping()
                        {
                            ActID = detail.ActID,
                            TypeID = (int)detail.CompanyTypeID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };

                        if (!Act_CompanyTypeMappingExists(ACTM))
                        {
                            entities.Act_CompanyTypeMapping.Add(ACTM);
                            entities.SaveChanges();
                        }

                        Act_BusinessActivityMapping ABAM = new Act_BusinessActivityMapping()
                        {
                            ActID = detail.ActID,
                            BATypeID = (int)detail.BusinessActivityID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };
                        if (!ActBusinessActivityMappingExists(ABAM))
                        {
                            entities.Act_BusinessActivityMapping.Add(ABAM);
                            entities.SaveChanges();
                        }


                        Act_LoctionTypeMapping ALTM = new Act_LoctionTypeMapping()
                        {
                            ActID = detail.ActID,
                            LoctionTypeID = (int)detail.LocationTypeID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };

                        if (!Act_LoctionTypeMappingExists(ALTM))
                        {
                            entities.Act_LoctionTypeMapping.Add(ALTM);
                            entities.SaveChanges();
                        }


                        Act_ActApplicabilityMapping AAAM = new Act_ActApplicabilityMapping()
                        {
                            ActID = detail.ActID,
                            Act_ApplicabilityID = (int)detail.ActApplicabilityID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                        };
                        if (!ActApplicabilityMappingExists(AAAM))
                        {
                            entities.Act_ActApplicabilityMapping.Add(AAAM);
                            entities.SaveChanges();
                        }
                        var query = (from row in entities.Acts
                                     where row.ID == detail.ActID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.LocationTypeID = detail.LocationTypeID;
                            entities.SaveChanges();
                        }

                    }
                    result = "Details Saved Successfully!";
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Tagged  Question  ACT Mapping
        public JsonResult getAllTaggedActs(int actgroupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.sp_TaggedGetActdetails(actgroupid,AuthenticationHelper.UserID)
                                    select usr).ToList();        
                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        

     
        public string UpdateActTaggedQuestion(QA_MappingUpdate detail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var query = (from row in entities.Questions_ActMapping
                                     where row.IsActive == false
                                          && row.Id == detail.TaggedQACMMappingID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.ActId = detail.TaggedActID;
                            query.QuestionID = detail.TaggedQuestionID;
                            query.ControllId = detail.TaggedControllID;
                            query.AnswerTOQ = detail.TaggedServiceID;
                            query.AnswerValue = detail.TaggedAnswer;
                            if (detail.TaggedControllID == 1)
                            {
                                query.AnsweSingleMulti = detail.TaggedAnswerSingle;
                            }
                            else if (detail.TaggedControllID == 2)
                            {
                                query.AnsweSingleMulti = detail.TaggedAnswerMultiple;
                            }
                            query.StatusId = 2;
                            query.AnsweRangeMin = detail.TaggedAnsweRangeMin;
                            query.AnsweRangeMax = detail.TaggedAnsweRangeMax;
                            query.UpdatedOn = DateTime.Now;
                            query.Updatedby = AuthenticationHelper.UserID;
                            query.LocationTypeID=detail.TaggedLocationTypeID;
                            entities.SaveChanges();
                            Act_CompanyTypeMapping ACTM = new Act_CompanyTypeMapping()
                            {
                                ActID = detail.TaggedActID,
                                TypeID = (int)detail.TaggedCompanyTypeID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!Act_CompanyTypeMappingExists(ACTM))
                            {
                                entities.Act_CompanyTypeMapping.Add(ACTM);
                                entities.SaveChanges();
                            }      
                            Act_BusinessActivityMapping ABAM = new Act_BusinessActivityMapping()
                            {
                                ActID = detail.TaggedActID,
                                BATypeID = (int)detail.TaggedBusinessActivityID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!ActBusinessActivityMappingExists(ABAM))
                            {
                                entities.Act_BusinessActivityMapping.Add(ABAM);
                                entities.SaveChanges();
                            }
                            Act_LoctionTypeMapping ALTM = new Act_LoctionTypeMapping()
                            {
                                ActID = detail.TaggedActID,
                                LoctionTypeID = (int)detail.TaggedLocationTypeID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };

                            if (!Act_LoctionTypeMappingExists(ALTM))
                            {
                                entities.Act_LoctionTypeMapping.Add(ALTM);
                                entities.SaveChanges();
                            }

                            Act_ActApplicabilityMapping AAAM = new Act_ActApplicabilityMapping()
                            {
                                ActID = detail.TaggedActID,
                                Act_ApplicabilityID = (int)detail.TaggedActApplicabilityID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!ActApplicabilityMappingExists(AAAM))
                            {
                                entities.Act_ActApplicabilityMapping.Add(AAAM);
                                entities.SaveChanges();
                            }
                            var query1 = (from row in entities.Acts
                                         where row.ID == detail.TaggedActID
                                          select row).FirstOrDefault();
                            if (query1 != null)
                            {
                                query1.LocationTypeID = detail.TaggedLocationTypeID;
                                entities.SaveChanges();
                            }


                            result = "Details Updated Successfully!";
                            dbContextTransaction.Commit();
                        }
                        else
                        {
                            result = "Question Not Found!";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }


        [HttpPost]
        public ActionResult BulkUpdateActTaggedQuestion(QAMappingEntryUpdate bupdate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (QA_MappingUpdate detail in bupdate.BulkUpdates)
                    {
                        var query = (from row in entities.Questions_ActMapping
                                     where row.IsActive == false
                                          && row.Id == detail.TaggedQACMMappingID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.ActId = detail.TaggedActID;
                            query.QuestionID = detail.TaggedQuestionID;
                            query.ControllId = detail.TaggedControllID;
                            query.AnswerTOQ = detail.TaggedServiceID;
                            query.AnswerValue = detail.TaggedAnswer;
                            if (detail.TaggedControllID == 1)
                            {
                                query.AnsweSingleMulti = detail.TaggedAnswerSingle;
                            }
                            else if (detail.TaggedControllID == 2)
                            {
                                query.AnsweSingleMulti = detail.TaggedAnswerMultiple;
                            }
                            query.StatusId = 2;
                            query.AnsweRangeMin = detail.TaggedAnsweRangeMin;
                            query.AnsweRangeMax = detail.TaggedAnsweRangeMax;
                            query.UpdatedOn = DateTime.Now;
                            query.Updatedby = AuthenticationHelper.UserID;
                            query.LocationTypeID = detail.TaggedLocationTypeID;
                            entities.SaveChanges();

                            Act_CompanyTypeMapping ACTM = new Act_CompanyTypeMapping()
                            {
                                ActID = detail.TaggedActID,
                                TypeID = (int)detail.TaggedCompanyTypeID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!Act_CompanyTypeMappingExists(ACTM))
                            {
                                entities.Act_CompanyTypeMapping.Add(ACTM);
                                entities.SaveChanges();
                            }
                            Act_BusinessActivityMapping ABAM = new Act_BusinessActivityMapping()
                            {
                                ActID = detail.TaggedActID,
                                BATypeID = (int)detail.TaggedBusinessActivityID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!ActBusinessActivityMappingExists(ABAM))
                            {
                                entities.Act_BusinessActivityMapping.Add(ABAM);
                                entities.SaveChanges();
                            }

                            Act_LoctionTypeMapping ALTM = new Act_LoctionTypeMapping()
                            {
                                ActID = detail.TaggedActID,
                                LoctionTypeID = (int)detail.TaggedLocationTypeID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };

                            if (!Act_LoctionTypeMappingExists(ALTM))
                            {
                                entities.Act_LoctionTypeMapping.Add(ALTM);
                                entities.SaveChanges();
                            }

                            Act_ActApplicabilityMapping AAAM = new Act_ActApplicabilityMapping()
                            {
                                ActID = detail.TaggedActID,
                                Act_ApplicabilityID = (int)detail.TaggedActApplicabilityID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedDate = DateTime.Now,
                            };
                            if (!ActApplicabilityMappingExists(AAAM))
                            {
                                entities.Act_ActApplicabilityMapping.Add(AAAM);
                                entities.SaveChanges();
                            }
                            var query1 = (from row in entities.Acts
                                          where row.ID == detail.TaggedActID
                                          select row).FirstOrDefault();
                            if (query1 != null)
                            {
                                query1.LocationTypeID = detail.TaggedLocationTypeID;
                                entities.SaveChanges();
                            }
                        }
                    }
                    result = "Details Updated Successfully!";
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}