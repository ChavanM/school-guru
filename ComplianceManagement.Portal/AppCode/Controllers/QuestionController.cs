﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class QuestionController : Controller
    {
        // GET: Question
        public ActionResult Index()
        {
            return View();
        }       
        public ActionResult QuestionsMaster()
        {
            return View();
        }
        //GetActAgrup

        [HttpGet]
        public ActionResult GetActAgrup()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;

                var role = (from A in entities.IMA_GetAssignedAct(AuthenticationHelper.UserID, "ACTGROUP", "All")
                            select new
                            {
                                ID = A.ID,
                                Name = A.Name,
                                ActGroupID = A.ActGroupID
                            }).ToList();

                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetAct(int groupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;

                var role = (from A in entities.IMA_GetAssignedAct(AuthenticationHelper.UserID, "ACT", "All")
                            where A.ActGroupID == groupid
                            select new
                            {
                                ID = A.ID,
                                Name = A.Name,
                                ActGroupID = A.ActGroupID
                            }).ToList();


                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetQuestionsACTDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> d = new List<int>();
                d.Add(2);
                d.Add(3);
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.QuestionMasters.Where(x => d.Contains((int)x.IsQuestionType) && x.IsActive == false).Select(model => new { model.ID, model.QuestionName }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetControllsfromQuestion(int qID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var master = (from data in entities.QuestionMasters
                              where data.ID == qID
                              select data
                            ).FirstOrDefault();
                return Json(master, JsonRequestBehavior.AllowGet);
              
            }
        }
        
        public ActionResult ComplianceApplicabilityQuestions()
        {
            return View();
        }
        
        public ActionResult ActApplicabilityQuestions()
        {
            return View();
        }

        #region Question Master
        public ActionResult CheckExistance(string QuestionName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool isExist = entities.QuestionMasters.Where(x => x.IsActive == false && x.QuestionName.ToUpper().Trim() == QuestionName.ToUpper().Trim()).Count() > 0 ? true : false;
                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getAllQuestion()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.GetAllQuestionData()                               
                                    select usr).ToList();
             
                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public JsonResult getQuestionByNo(string id)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    int no = Convert.ToInt32(id);
                    QuestionMasterModal abc = new QuestionMasterModal();
                    var taskList = (from i in entities.QuestionMasters
                                    where i.ID == no
                                    select i).FirstOrDefault();
                    if (taskList != null)
                    {
                        abc.ID = (Int32)taskList.ID;
                        abc.QuestionName = taskList.QuestionName;
                        abc.ControlID = (Int32)taskList.ControlId;
                        abc.TabID = (Int32)taskList.TabID;
                        abc.IsQuestionType = (Int32)taskList.IsQuestionType;
                        var aa = taskList.PossibleAnswer.Split(',');
                        List<PossibleAnswers> Listval = new List<PossibleAnswers>();
                        if (aa.Length > 0)
                        {
                            foreach (var item in aa)
                            {
                                PossibleAnswers val = new PossibleAnswers();
                                val.name = item;
                                Listval.Add(val);
                            }
                        }
                        abc.PossibleAnswer = Listval;
                    }
                    return Json(abc, JsonRequestBehavior.AllowGet);
                }
                catch (Exception exp)
                {
                    LoggerMessage.InsertLog(exp, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return Json("Error in getting record !", JsonRequestBehavior.AllowGet);
                }
            }
        }
        public string AddQuestion(QuestionMasterModal act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var PAnser = string.Empty;
                        if (act.PossibleAnswer != null)
                        {
                            List<string> activityGuids = new List<string>();
                            foreach (var item in act.PossibleAnswer)
                            {
                                activityGuids.Add(item.name);
                            }
                            PAnser = String.Join(",", activityGuids.ToArray());
                        }
                        QuestionMaster Qmst = new QuestionMaster();
                        Qmst.ID = 0;
                        Qmst.QuestionName = act.QuestionName;
                        Qmst.ControlId = act.ControlID;
                        Qmst.TabID = act.TabID;
                        Qmst.PossibleAnswer = PAnser;
                        Qmst.IsQuestionType = act.IsQuestionType;
                        Qmst.CreatedOn = DateTime.Now;
                        Qmst.CreatedBy = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        if (QuestionExists(Qmst))
                        {
                            result = "Question name already exists";
                        }
                        else
                        {
                            entities.QuestionMasters.Add(Qmst);
                            entities.SaveChanges();
                            result = "Question Saved Successfully!";
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }
        public string UpdateQuestion(QuestionMasterModal act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {

                        var query = (from row in entities.QuestionMasters
                                     where row.IsActive == false
                                          && row.ID==act.ID
                                     select row).FirstOrDefault();
                        if (query !=null)
                        {
                            var PAnser = string.Empty;
                            if (act.PossibleAnswer != null)
                            {
                                List<string> activityGuids = new List<string>();
                                foreach (var item in act.PossibleAnswer)
                                {
                                    activityGuids.Add(item.name);
                                }
                                PAnser = String.Join(",", activityGuids.ToArray());
                            }

                            query.QuestionName = act.QuestionName;
                            query.ControlId = act.ControlID;
                            query.TabID = act.TabID;
                            query.PossibleAnswer = PAnser;
                            query.IsQuestionType = act.IsQuestionType;
                            query.UpdatedOn = DateTime.Now;
                            query.UpdateBy = AuthenticationHelper.UserID;
                            entities.SaveChanges();
                            result = "Question Updated Successfully!";
                            dbContextTransaction.Commit();
                        } 
                        else
                        {
                            result = "Question Not Found!";
                        }                                               
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }
        //Fetching AnswerControl Value from Database
        [HttpGet]
        public ActionResult GetAnswerControl()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.QuestionAnswerControls.Where(x => x.IsActive == false).Select(model => new { model.ID, model.ControlName }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        public string DeleteQuestion(string Id)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!String.IsNullOrEmpty(Id))
                {
                    try
                    {
                        using (var dbContextTransaction = entities.Database.BeginTransaction())
                        {
                            try
                            {
                                var aa = Convert.ToInt32(Id);
                                var prevRecord = (from row in entities.QuestionMasters
                                                  where row.ID == aa
                                                  select row).FirstOrDefault();
                                if (prevRecord != null)
                                {
                                    prevRecord.IsActive = true;
                                    entities.SaveChanges();
                                }

                                dbContextTransaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                return ex.InnerException.ToString();
                            }
                        }
                        return "Record deleted sucessfully";
                    }
                    catch (Exception)
                    {
                        return "Details not found";
                    }
                }
                else
                {
                    return "Invalid operation";
                }
            }
        }
        public bool QuestionExists(QuestionMaster customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.QuestionMasters
                             where row.IsActive == false
                             && row.ControlId==customer.ControlId
                             && row.QuestionName.ToUpper().Trim().Equals(customer.QuestionName.ToUpper().Trim())
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        #endregion




        #region not Used Code
        public ActionResult TestAshaQuestions()
        {
            return View();
        }
        

        [HttpGet]
        public ActionResult GetCompliance(string aa)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.Compliances.Where(x => x.IsDeleted == false).Select(model => new { model.ID, model.ShortDescription }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        //Fetching RoleManager Value from Database
        [HttpGet]
        public ActionResult GetMastersfromDB()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.QuestionAnswarMasters.Where(x => x.IsActive == false && x.IsForSubMaster == null).Select(model => new { model.ID, model.MasterName }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetSubMastersDataAll(int MID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var master = (from data in entities.sp_ImplementationAutomationSubFillData(MID)
                              select data
                            ).Select(model => new { model.ID, model.Name }).ToList();


                return Json(master, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetMastersDataAll(int MID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                if (MID != -1)
                {
                    var master = (from data in entities.sp_ImplementationAutomationFillData(MID)
                                  select data
                            ).Select(model => new { model.ID, model.Name }).ToList();
                    return Json(master, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpGet]
        public ActionResult GetMastersfromDBAll(int MID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var master = (from data in entities.QuestionAnswarMasters
                              where data.ID == MID
                              select data
                            ).FirstOrDefault();


                return Json(master, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult KendoQuestions()
        {
            return View();
        }
        public ActionResult QuestionsMasterTest()
        {
            return View();
        }
        public ActionResult TestQuestions()
        {
            return View();
        }
        public ActionResult KendoTagQuestions()
        {
            return View();
        }
        public ActionResult TestRahulQuestions()
        {
            return View();
        }
        public ActionResult TestRahul()
        {
            return View();
        }
        public ActionResult Indexdemo()
        {
            return View();
        }

        
        //<--Activity record insertion code -->  
        public JsonResult getAll1()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.sp_GetActdetails()
                                    select usr).ToList();

                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public string AddActivity(QuestionModalEntry act)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var questionName = act.customerassignments.Select(entry => entry.QuestionName).FirstOrDefault();
                        QuestionMaster Qmst = new QuestionMaster();
                        Qmst.ID = 0;
                        Qmst.QuestionName = questionName;
                        Qmst.CreatedOn = DateTime.Now;
                        Qmst.CreatedBy = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;

                        //if (Exists(Qmst))
                        if (true)
                        {
                            result = "Question name already exists";
                        }
                        else
                        {
                            entities.QuestionMasters.Add(Qmst);
                            entities.SaveChanges();

                            var QuestionID = Qmst.ID;
                           
                            result = "Question has been Created Successfully!";
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }

        //<-- Get Activity by ID -->

        public string UpdateActivity(QuestionModalEntry act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var questionName = act.customerassignments.Select(entry => entry.QuestionName).FirstOrDefault();
                        QuestionMaster Qmst = new QuestionMaster();
                        Qmst.ID = 0;
                        Qmst.QuestionName = questionName;
                        Qmst.CreatedOn = DateTime.Now;
                        Qmst.CreatedBy = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        //if (Exists(Qmst))
                        if (true)
                        {
                            result = "Question name already exists";
                        }
                        else
                        {

                            entities.QuestionMasters.Add(Qmst);
                            entities.SaveChanges();
                            var QuestionID = Qmst.ID;

                          

                            result = "Question has been Updated Successfully!";
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }
        //Delete Activity  by Id
        public class LinkedToALicense
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        public class demodetails
        {
            public int ActID { get; set; }
            public string ActName { get; set; }
            public long ComplianceID { get; set; }
        }
        
        public class DemoDummyClass
        {
            public int ActID { get; set; }
            public string ActName { get; set; }
            public long ComplianceID { get; set; }
            //public demodetails Expenses { get; set; }
            public List<LinkedToALicense> LTADetails { get; set; }
            public List<LinkedToALicenseType> LinkedToALicenseTypeDetails { get; set; }
        }
        public JsonResult getAllCompliancesNew(int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<DemoDummyClass> details = new List<DemoDummyClass>();
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.sp_GetActCompliancedetails(actid)
                                    select usr).ToList();


                List<LinkedToALicense> clist = new List<LinkedToALicense>();
                LinkedToALicense c = new LinkedToALicense();
                c.ID = 1;
                c.Name = "Yes";
                clist.Add(c);
                LinkedToALicense c1 = new LinkedToALicense();
                c1.ID = 2;
                c1.Name = "No";
                clist.Add(c1);

                List<LinkedToALicenseType> expansedetails = new List<LinkedToALicenseType>();
                var expansedetail = entities.Lic_tbl_LicenseType_Master.Where(x => x.IsDeleted == false).Select(model => new { model.ID, model.Name }).ToList();
                foreach (var detail in expansedetail)
                {
                    expansedetails.Add(new LinkedToALicenseType
                    {
                        LID = detail.ID,
                        LName = detail.Name,
                        LinkedID = 1,
                    });
                }
                LinkedToALicenseType cc = new LinkedToALicenseType();
                cc.LID = 0;
                cc.LName = "No";
                cc.LinkedID = 2;
                expansedetails.Add(cc);

                foreach (var item in activityList)
                {
                    DemoDummyClass detail = new DemoDummyClass();

                    demodetails x = new demodetails();
                    x.ActID = item.ActID;
                    x.ActName = item.ActName;
                    x.ComplianceID = item.ComplianceID;

                    detail.ActID = item.ActID;
                    detail.ActName = item.ActName;
                    detail.ComplianceID = item.ComplianceID;

                    //detail.Expenses = x;
                    detail.LTADetails = clist;
                    detail.LinkedToALicenseTypeDetails = expansedetails;
                    details.Add(detail);
                }
                var JsonResult = Json(details, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        #endregion
    }
}