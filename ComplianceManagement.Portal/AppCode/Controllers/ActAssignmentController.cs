﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class ActAssignmentController : Controller
    {
        // GET: ActAssignment
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetPerformerName()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var empList = (from user in entities.Users
                               where user.RoleID==12 && user.IsDeleted==false
                               select new
                               {
                                   ID = user.ID,
                                   Name = user.FirstName + " " + user.LastName,
                               }).ToList().OrderBy(user => user.Name);



                var JsonResult = Json(empList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        [HttpGet]
        public ActionResult GetReviewerName()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var empList = (from user in entities.QuestionReviewers                            
                               select new
                               {
                                   ID = user.Userid,
                                   Name = user.Name,
                               }).ToList().OrderBy(user => user.Name);



                var JsonResult = Json(empList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }

        
        public JsonResult getAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;

                var CsutomersList = (from custass in entities.IMA_AllSavedActs()
                                     select custass).ToList();

                var JsonResult = Json(CsutomersList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        //[HttpGet]
        //public ActionResult GetActName()
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //   {              
        //        entities.Configuration.ProxyCreationEnabled = false;
        //        var cust = entities.Acts.Where(Am=> Am.IsDeleted==false).Select(model => new { model.ID, model.Name }).ToList().OrderBy(model => model.Name);                
        //        return Json(cust, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpGet]
        public ActionResult GetActName()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;                
                var CsutomersList = (from custass in entities.IMA_GetAllActs()
                                     select custass).ToList().Select(model => new { ID= model.ID, Name= model.ActName }).ToList().OrderBy(model => model.Name); ;

                var JsonResult = Json(CsutomersList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public JsonResult getCustomerAssignmentByNo(string id)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int no = Convert.ToInt32(id);
                    //var custList = (from usr in entities.ImplementationActAssignments
                    //                where usr.PerformerID == no
                    //                select new
                    //                {
                    //                    usr.ID,
                    //                    usr.PerformerID,
                    //                    usr.ActID,
                    //                }).ToList();

                    var custList = (from custass in entities.IMA_AllSavedActs()
                                         where custass.ID == no
                                         select custass).ToList();
                    return Json(custList, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                LoggerMessage.InsertLog(exp, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json("Error in getting record !", JsonRequestBehavior.AllowGet);
            }
        }
        public string DeleteCustomerAssignment(string Id)
        {
            if (!String.IsNullOrEmpty(Id))
            {
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        using (var dbContextTransaction = entities.Database.BeginTransaction())
                        {
                            try
                            {
                                int cId = Int32.Parse(Id);
                                entities.Database.ExecuteSqlCommand("delete from ImplementationActAssignment where ID=" + cId);
                                entities.SaveChanges();
                                dbContextTransaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                dbContextTransaction.Rollback();
                                return ex.InnerException.ToString();
                            }
                        }
                        return "Selected Act assignment record deleted sucessfully";
                    }
                }
                catch (Exception ex1)
                {
                    return ex1.InnerException.ToString();
                }
            }
            else
            {
                return "Invalid operation";
            }
        }

        public string DeleteCustomerAssignment1(string Id, string EId)
        {
            if (!String.IsNullOrEmpty(Id))
            {
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        using (var dbContextTransaction = entities.Database.BeginTransaction())
                        {
                            try
                            {
                                int eId = Int32.Parse(EId);
                                entities.Database.ExecuteSqlCommand("delete from ImplementationActAssignment where EmployeeID=" + eId);

                                entities.SaveChanges();
                                dbContextTransaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                dbContextTransaction.Rollback();
                                return ex.InnerException.ToString();
                            }
                        }
                        return "Selected Act assignment record deleted sucessfully";
                    }
                }
                catch (Exception ex1)
                {
                    return ex1.InnerException.ToString();
                }
            }
            else
            {
                return "Invalid operation";
            }
        }
        public string AddCustomerAssignment(ImplementationActAssignment Customers)
        {
            if (Customers != null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (var dbContextTransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            ImplementationActAssignment cust = new ImplementationActAssignment();
                            cust.ID = Customers.ID;
                            cust.PerformerID = Customers.PerformerID;
                            cust.ReviewerID = Customers.ReviewerID;
                            cust.ActID = Customers.ActID;
                            cust.CreatedBy = AuthenticationHelper.UserID;
                            cust.CreatedOn = DateTime.Now;
                            entities.ImplementationActAssignments.Add(cust);
                            entities.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            dbContextTransaction.Rollback();
                            return ex.GetType().ToString();
                        }
                        return "Act Assignment added Successfully";
                    }
                }
            }
            else
            {
                return "Addition of  Act Assignment unsucessfull !";
            }
        }
        public string UpdateCustomerAssignment(ImplementationActAssignment ca)
        {
            int CLI_Id = Convert.ToInt32(Session["CLI_Id"]);
            if (ca != null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (var dbContextTransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            int no = Convert.ToInt32(ca.ID);
                            var cust = entities.ImplementationActAssignments.Where(x => x.ID == no).FirstOrDefault();
                            cust.ID = ca.ID;
                            cust.PerformerID = ca.PerformerID;
                            cust.ReviewerID = ca.ReviewerID;
                            cust.ActID = ca.ActID;
                            cust.CreatedBy = AuthenticationHelper.UserID;
                            cust.CreatedOn = DateTime.Now;
                            entities.ImplementationActAssignments.Add(cust);
                            entities.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            dbContextTransaction.Rollback();
                            return ex.GetType().ToString();
                        }
                        return "Act Assignment Updated Successfully";
                    }
                }
            }
            else
            {
                return "Invalid  Act Assignment";
            }
        }
    
        public ActionResult SaveCusAssignments(ActAssignmentEntry Customers)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Act assignment  Is Not Complete!";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {                        
                        foreach (ImplementationActAssignment cus in Customers.actassignments)
                        {
                            var EId = entities.ImplementationActAssignments.Where(x => x.PerformerID == cus.PerformerID && x.ActID==cus.ActID && x.IsActive==false).FirstOrDefault();
                            if (EId == null)
                            {
                                ImplementationActAssignment cust = new ImplementationActAssignment();
                                cust.ID = cus.ID;
                                cust.ActID = cus.ActID;
                                cust.PerformerID = cus.PerformerID;
                                cust.ReviewerID = cus.ReviewerID;
                                cust.CreatedBy = AuthenticationHelper.UserID;
                                cust.CreatedOn = DateTime.Now;
                                entities.ImplementationActAssignments.Add(cust);
                                entities.SaveChanges();
                            }
                            else
                            {
                                EId.ReviewerID = cus.ReviewerID;
                                entities.SaveChanges();                              
                            }
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        dbContextTransaction.Rollback();
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                    result = "Act assignment has been Created Successfully!";
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateSaveCusAssignments(ActAssignmentEntry Customers)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Act assignment  Is Not Complete!";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (ImplementationActAssignment cus in Customers.actassignments)
                        {
                            var EId = entities.ImplementationActAssignments.Where(x => x.ID == cus.ID && x.ActID==cus.ActID  && x.IsActive == false).FirstOrDefault();
                            if (EId == null)
                            {
                                ImplementationActAssignment cust = new ImplementationActAssignment();
                                cust.ID = cus.ID;
                                cust.ActID = cus.ActID;
                                cust.PerformerID = cus.PerformerID;
                                cust.ReviewerID = cus.ReviewerID;
                                cust.CreatedBy = AuthenticationHelper.UserID;
                                cust.CreatedOn = DateTime.Now;
                                entities.ImplementationActAssignments.Add(cust);
                                entities.SaveChanges();
                            }
                            else
                            {
                                EId.ActID = cus.ActID;
                                EId.PerformerID = cus.PerformerID;
                                EId.ReviewerID = cus.ReviewerID;
                                EId.ReviewerID = cus.ReviewerID;
                                entities.SaveChanges();
                            }
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        dbContextTransaction.Rollback();
                        return Json(ex.Message, JsonRequestBehavior.AllowGet);
                    }
                    result = "Act assignment has been Created Successfully!";
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}