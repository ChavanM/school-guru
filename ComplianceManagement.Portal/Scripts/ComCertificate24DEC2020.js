 function exportReport(e)
        {
   
            if (document.getElementById('IsLabel').value == 1) {

                var ReportName = "Report of Compliances";
                var customerName = document.getElementById('CustName').value;
                // var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                var todayDate = moment().format('DD-MMM-YYYY');
                var grid = $("#grid").getKendoGrid();

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: customerName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:",  bold: true },
                            { value: todayDate } ,
                         
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "Compliance ID", bold: true },
                            { value: "Location", bold: true },
                            { value: "Act Name", bold: true },
                            { value: "Short Description", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Reviewer", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "For Month", bold: true },
                            { value: "Status", bold: true },
                            { value: "Risk Category", bold: true },
                            { value: "Label", bold: true }
                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [
                            { value: dataItem.ComplianceID },
                            { value: dataItem.Branch },
                            { value: dataItem.Name },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.Performer },
                            { value: dataItem.Reviewer },
                            { value: dataItem.ScheduledOn, format:"dd-MMM-yyyy" },
                            { value: dataItem.ForMonth },
                            { value: dataItem.Status },
                            { value: dataItem.RiskCategory },
                            { value: dataItem.SequenceID }
                        ]
                    });
                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 10; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;
                    }
                }
                excelExport(rows, ReportName);
                e.preventDefault();
            }
            else {

                var ReportName = "Report of Compliances";
                var customerName = document.getElementById('CustName').value;
              //  var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                //  todayDate.Cells(x,y).NumberFormat = 'dd-mm-yyyy';
                var todayDate = moment().format('DD-MMM-YYYY');
                var grid = $("#grid").getKendoGrid();

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: customerName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate } ,
                          //  {date:"dd-MM-yy"}
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                              { value: "Compliance ID", bold: true },
                            { value: "Location", bold: true },
                            { value: "Act Name", bold: true },
                            { value: "Short Description", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Reviewer", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "For Month", bold: true },
                            { value: "Status", bold: true },
                            { value: "Risk Category", bold: true }
                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [
                              { value: dataItem.ComplianceID },
                            { value: dataItem.Branch },
                            { value: dataItem.Name },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.Performer },
                            { value: dataItem.Reviewer },
                            { value: dataItem.ScheduledOn,format:"dd-MMM-yyyy" },
                            { value: dataItem.ForMonth },
                            { value: dataItem.Status },
                            { value: dataItem.RiskCategory },
                        ]
                    });
                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 9; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;
                    }
                }
                excelExport(rows, ReportName);
                e.preventDefault();

            }

        }

        function excelExport(rows, ReportName) {


            if (document.getElementById('IsLabel').value == 1)
            {
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                        {
                            columns: [
                                { width: 180 },
                                { width: 180 },
                                { width: 300 },
                                { width: 300 },
                                { width: 180 },
                                { width: 180 },
                                { width: 100 },
                                { width: 100 },
                                { width: 150 },
                                { width: 100 },
                                { width: 100 }
                            ],
                            title: "Report",
                            rows: rows
                        },
                    ]
                });

                var nameOfPage = "Report";
                kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            }
            else {
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                        {
                            columns: [
                                { width: 180 },
                                { width: 180 },
                                { width: 180 },
                                { width: 180 },
                                { width: 300 },
                                { width: 300 },
                                { width: 180 },
                                { width: 180 },
                                { width: 100 },
                                { width: 100 },
                                { width: 150 },
                                { width: 100 }
                            ],
                            title: "Report",
                            rows: rows
                        },
                    ]
                });

                var nameOfPage = "Report";
                kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            }
        }

function FilterGridDemo() {
    //location details
    var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
    var locationsdetails = [];
    $.each(list1, function (i, v) {
        locationsdetails.push({
            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
        });
    });

    //risk Details
    var Riskdetails = [];
    var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
    $.each(list3, function (i, v) {
        Riskdetails.push({
            field: "Risk", operator: "eq", value: parseInt(v)
        });
    });


    //Status Details
    var Statusdetails = [];
    var liststatus = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
    $.each(liststatus, function (i, v) {
        Statusdetails.push({
            field: "Status", operator: "eq", value: v
        });
    });

    //Act Details
    var Actdetails = [];
    if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
        Actdetails.push({
            field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
        });
    }

    //Function Details
    var Functiondetails = [];    
    if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {        
        Functiondetails.push({
            field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
        });
    }

    //datefilter
    var datedetails = [];
    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
        });
    }
    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
        });
    }

    var dataSource = $("#grid").data("kendoGrid").dataSource;

    //six
    if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Statusdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    //five
    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Statusdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Statusdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Statusdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Statusdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Statusdetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (Riskdetails.length > 0
        && Statusdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    //four
    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    //three
    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Functiondetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Functiondetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }



    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Functiondetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (Riskdetails.length > 0
        && Statusdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }


    else if (Riskdetails.length > 0
        && Statusdetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (Riskdetails.length > 0
        && Statusdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (Statusdetails.length > 0
        && Functiondetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (Statusdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    //two
    else if (locationsdetails.length > 0
        && Riskdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Functiondetails.length > 0
        && Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (Functiondetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Actdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Functiondetails.length > 0
        && Statusdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    else if (Actdetails.length > 0
        && Statusdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    else if (datedetails.length > 0
        && Statusdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "and",
                    filters: datedetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Statusdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    //one
    else if (Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }
    else if (Actdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }
    else if (datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }
    else if (locationsdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }
    else if (Riskdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }
    else if (Statusdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }
    else {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }
}

function ClearAllFilterMain(e) {
    $("#dropdowntree").data("kendoDropDownTree").value([]);
    $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
    $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
    $("#dropdownUser").data("kendoDropDownTree").value([]);
    $("#grid").data("kendoGrid").dataSource.filter({});
  
    e.preventDefault();
}

function fcloseStory(obj) {

    var DataId = $(obj).attr('data-Id');
    var dataKId = $(obj).attr('data-K-Id');
    var seq = $(obj).attr('data-seq');
    var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
    $(deepspan).trigger('click');
    var upperli = $('#' + dataKId);
    $(upperli).remove();

    //for rebind if any pending filter is present (Main Grid)
    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
    fCreateStoryBoard('dropdownUser', 'filterUser1', 'user');
    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
    fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
    fCreateStoryBoard('dropdownSequence', 'filterCompSubType', 'sequence');
    fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');
    fCreateStoryBoard('dropdownFY', 'filterFY', 'FY');
    CheckFilterClearorNotMain();
};

function CheckFilterClearorNotMain() {
   
    if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
        //$('#ClearfilterMain').css('display', 'none');
    }
}


//function fCreateStoryBoard(Id, div, filtername) {

//    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
//    $('#' + div).html('');
//    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
//    $('#' + div).css('display', 'block');

//    if (div == 'filtersstoryboard') {
//        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;');Dashboard
//        $('#ClearfilterMain').css('display', 'block');
//    }
//    else if (div == 'filterUser1') {
//        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterrisk') {
//        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');Dashboard
//        $('#ClearfilterMain').css('display', 'block');
//    }
//    else if (div == 'filterstatus') {
//        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');Dashboard
//        $('#ClearfilterMain').css('display', 'block');
//    }
//    else if (div == 'filterpstData1') {
//        $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterCategory') {
//        $('#' + div).append('Category&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterAct') {
//        $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterCompSubType') {
//        $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterCompType') {
//        $('#' + div).append('type&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filtersstoryboard1') {
//        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filtertype1') {
//        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterrisk1') {
//        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterFY') {
//        $('#' + div).append('FY&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterUser') {
//        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }
//    else if (div == 'filterstatus1') {
//        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
//        $('#Clearfilter').css('display', 'block');
//    }

//    for (var i = 0; i  $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
//        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
//        $(button).css('display', 'none');
//        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
//        var buttontest = $($(button).find('span')[0]).text();
//        if (buttontest.length  10) {
//            buttontest = buttontest.substring(0, 10).concat(...);
//        }
//        $('#' + div).append('li class=k-button  unselectable=on Id=ddl' + filtername + [i] + ' role=option style=background-color#EBEBEB; height 20px;ColorGray;border-radius10px;margin-left4px;margin-top 4px;vertical-align baseline;span unselectable=on title=' + $($(button).find('span')[0]).text() + '' + buttontest + 'spanspan data-K-Id=ddl' + filtername + [i] + ' data-Id=' + IdKReset + ' data-seq=' + i + ' onclick=fcloseStory(this) title=clear aria-label=clear class=k-select style=padding-left 6px;span data-Id=' + IdKReset + ' data-seq=' + i + '  onclick=fcloseStory(this)  class=k-icon k-i-close style=font-size 12px;spanspanli');
//        $('#' + div).append('li class=k-button  unselectable=on Id=ddl' + filtername + [i] + ' role=option style=background-color#EBEBEB; height 20px;ColorGray;span unselectable=on' + buttontest + 'spanspan data-K-Id=ddl' + filtername + [i] + ' data-Id=' + IdKReset + ' data-seq=' + i + ' onclick=fcloseStory(this) title=delete aria-label=delete class=k-selectspan data-Id=' + IdKReset + ' data-seq=' + i + '  onclick=fcloseStory(this)  class=k-icon k-i-closespanspanli');
//    }

//    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
//        $('#' + div).css('display', 'none');
//        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

//    }
//    CheckFilterClearorNotMain();
//}

//fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
//  fCreateStoryBoard('dropdownDept', 'filterDept', 'Dept');
function fCreateStoryBoard(Id, div, filtername) {
    debugger;
    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
    $('#' + div).html('');
    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
    $('#' + div).css('display', 'block');

    if (div == 'filtersstoryboard') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterUser1') {
        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterDept') {
        $('#' + div).append('Department&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterrisk') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterstatus') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterpstData1') {
        $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCategory') {
        $('#' + div).append('Category&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterfunction') {
        $('#' + div).append('Category&nbsp;&nbsp;:');
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterAct') {
        $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompSubType') {
        $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompType') {
        $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtersstoryboard1') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtertype1') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterrisk1') {
        $('#' + div).append('Risk&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterFY') {
        $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterUser') {
        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterstatus1') {
        $('#' + div).append('Status&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }

    for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
        $(button).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
        var buttontest = $($(button).find('span')[0]).text();
        if (buttontest.length > 10) {
            buttontest = buttontest.substring(0, 10).concat("...");
        }
        $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:1px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
        //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
    }

    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
        $('#' + div).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

    }
    CheckFilterClearorNotMain();
}