﻿function ClearAllFilterMain(e) {

    $("#dropdowntree").data("kendoDropDownTree").value([]);
    $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
    $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
    $('#ClearfilterMain').css('display', 'none');

    $('#dvbtndownloadDocumentMain').css('display', 'none');

    $("#dropdownEventName").data("kendoDropDownList").select(0);
    $("#dropdownEventNature").data("kendoDropDownList").select(0);

    $("#grid").data("kendoGrid").dataSource.filter({});

    $('input[id=chkAllMain]').prop('checked', false);

    e.preventDefault();
}

function CheckFilterClearorNotMain() {
    if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
        $('#ClearfilterMain').css('display', 'none');
    }
}

function CheckFilterClearorNot() {
    if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        //($($($('#dropdownACT').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistRisk1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        //($($($('#dropdownFY').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        //($($($('#dropdownPastData').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        //($($($('#dropdownlistComplianceType1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
        $('#Clearfilter').css('display', 'none');
    }
}

function fCreateStoryBoard(Id, div, filtername) {

    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
    $('#' + div).html('');
    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
    $('#' + div).css('display', 'block');

    if (div == 'filtersstoryboard') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filtertype') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
    }
    else if (div == 'filterrisk') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterstatus') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterpstData1') {
        $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCategory') {
        $('#' + div).append('Category&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterAct') {
        $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompSubType') {
        $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompType') {
        $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtersstoryboard1') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtertype1') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterrisk1') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterFY') {
        $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterUser') {
        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterstatus1') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }

    for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
        $(button).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
        var buttontest = $($(button).find('span')[0]).text();
        if (buttontest.length > 10) {
            buttontest = buttontest.substring(0, 10).concat("...");
        }
        $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
    }

    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
        $('#' + div).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

    }


    CheckFilterClearorNot();

    CheckFilterClearorNotMain();
}

function fcloseStory(obj) {

    var DataId = $(obj).attr('data-Id');
    var dataKId = $(obj).attr('data-K-Id');
    var seq = $(obj).attr('data-seq');
    var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
    $(deepspan).trigger('click');
    var upperli = $('#' + dataKId);
    $(upperli).remove();

    //for rebind if any pending filter is present (Main Grid)
    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
    //for rebind if any pending filter is present (ADV Grid)
    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
    fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');
    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
    //fCreateStoryBoard('dropdownACT', 'filterAct', 'Act');
    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');


    CheckFilterClearorNot();

    CheckFilterClearorNotMain();
};

function FilterAllMain() {

    var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
    var locationsdetails = [];
    $.each(list1, function (i, v) {
        locationsdetails.push({
            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
        });
    });

    //Status details
    var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
    var Statusdetails = [];
    $.each(list2, function (i, v) {
        Statusdetails.push({
            field: "Status", operator: "eq", value: v
        });
    });

    //risk Details
    var Riskdetails = [];
    var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
    $.each(list3, function (i, v) {
        Riskdetails.push({
            field: "Risk", operator: "eq", value: parseInt(v)
        });
    });


    var dataSource = $("#grid").data("kendoGrid").dataSource;

    if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
        && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
        && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }

    else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
        && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }

    else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
        && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }

    else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
        && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }

    else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    else if ($("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }

    else {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }
}

function FilterAllAdvancedSearch() {

    //location details
    var list1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
    var locationsdetails = [];
    $.each(list1, function (i, v) {
        locationsdetails.push({
            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
        });
    });

    //Status details
    var list2 = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
    var Statusdetails = [];
    $.each(list2, function (i, v) {
        Statusdetails.push({
            field: "Status", operator: "eq", value: v
        });
    });

    //risk Details
    var Riskdetails = [];
    var list3 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
    $.each(list3, function (i, v) {
        Riskdetails.push({
            field: "Risk", operator: "eq", value: parseInt(v)
        });
    });


    var RoleFlag = document.getElementById('RoleFlagCHK').value;
    //user Details
    var userdetails = [];
    if (RoleFlag == 1) {
        var list4 = $("#dropdownUser").data("kendoDropDownTree")._values;
        $.each(list4, function (i, v) {
            Riskdetails.push({
                field: "UserID", operator: "eq", value: parseInt(v)
            });
        });
    }
   
    //Act Details
    var Actdetails = [];
    if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
        Actdetails.push({
            field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
        });
    }

    //datefilter
    var datedetails = [];
    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
        });
    }
    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
        });
    }


    var dataSource = $("#grid1").data("kendoGrid").dataSource;
    //six
    if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }
    //five
    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Riskdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    //four
    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Riskdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Riskdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    //three

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Riskdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && userdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Statusdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && userdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Riskdetails.length > 0
        && userdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && userdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Riskdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                },
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Riskdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && userdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && userdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (userdetails.length > 0
        && Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "and",
                    filters: Actdetails
                },
                {
                    logic: "or",
                    filters: datedetails
                }
            ]
        });
    }

    //two
    else if (locationsdetails.length > 0
        && Statusdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && userdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Riskdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }


    else if (Statusdetails.length > 0
        && userdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (Statusdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && userdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: userdetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (userdetails.length > 0
        && Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }


    else if (locationsdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (userdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: userdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }


    else if (Actdetails.length > 0
        && datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Actdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    //one
    else if (Actdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Actdetails
                }
            ]
        });
    }

    else if (datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }

    else if (userdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: userdetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }

    else if (Statusdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Statusdetails
                }
            ]
        });
    }

    else {
        $("#grid1").data("kendoGrid").dataSource.filter({});
    }
}

function ChangeListView() {
    $('#grid1').css('display', 'block');
    $("#grid1").data("kendoGrid").showColumn(0);//Id
    $("#grid1").data("kendoGrid").hideColumn(1);//Risk
    $("#grid1").data("kendoGrid").hideColumn(2);//CId
    $("#grid1").data("kendoGrid").showColumn(3);//FileName
    $("#grid1").data("kendoGrid").showColumn(4);//V
    $("#grid1").data("kendoGrid").hideColumn(5);//Branch
    $("#grid1").data("kendoGrid").hideColumn(6);//SD
    $("#grid1").data("kendoGrid").hideColumn(7);//Scheduleon
    $("#grid1").data("kendoGrid").hideColumn(8);//ForMonth
    $("#grid1").data("kendoGrid").hideColumn(9);//Status
    $("#grid1").data("kendoGrid").showColumn(10);//VD
    $("#grid1").data("kendoGrid").hideColumn(11);//type
    $("#grid1").data("kendoGrid").showColumn(12);//Uploaded Date
    $("#grid1").data("kendoGrid").showColumn(13);//Size
    $("#grid1").data("kendoGrid").hideColumn(15);//Size

    $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
    $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
}

function ChangeView() {
    $('#grid1').css('display', 'block');

    $("#grid1").data("kendoGrid").showColumn(0);//Id
    $("#grid1").data("kendoGrid").hideColumn(1);//Risk
    $("#grid1").data("kendoGrid").hideColumn(2);//CId
    $("#grid1").data("kendoGrid").hideColumn(3);//FileName
    $("#grid1").data("kendoGrid").hideColumn(4);//V
    $("#grid1").data("kendoGrid").showColumn(5);//Branch
    $("#grid1").data("kendoGrid").showColumn(6);//SD
    $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
    $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
    $("#grid1").data("kendoGrid").showColumn(9);//Scheduleon
    $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
    $("#grid1").data("kendoGrid").showColumn(11);//Status
    $("#grid1").data("kendoGrid").hideColumn(12);//VD
    $("#grid1").data("kendoGrid").hideColumn(13);//type
    $("#grid1").data("kendoGrid").hideColumn(14);//Uploaded Date
    $("#grid1").data("kendoGrid").hideColumn(15);//Size

    $("#grid1").data("kendoGrid").hideColumn(16);//Size

    if ($("#dropdownlistComplianceType1").val() == 1)//event based
    {
        $("#grid1").data("kendoGrid").showColumn(7);//Event Name
        $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

        $("#grid1").data("kendoGrid").hideColumn(5);//Branch
        $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
    }

}

function exportReport(e) {
    var customerName = document.getElementById('CustName').value;
    var ReportName = "";
    if ($("#dropdownlistComplianceType").val() == -1) {
        ReportName = "Statutory";
    }
    if ($("#dropdownlistComplianceType").val() == 0) {
        ReportName = "Internal";
    }
    if ($("#dropdownlistComplianceType").val() == 1) {
        ReportName = "Event Based";
    }
    if ($("#dropdownlistComplianceType").val() == 2) {
        ReportName = "Statutory Checklist";
    }
    if ($("#dropdownlistComplianceType").val() == 3) {
        ReportName = "Internal Checklist";
    }
    if ($("#dropdownlistComplianceType").val() == 4) {
        ReportName = "Event Based Checklist";
    }

    var ReportNameAdd = "Report of " + ReportName + " Compliances";

    var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    var grid = $("#grid").getKendoGrid();

    //Statutory
    if ($("#dropdownlistComplianceType").val() == -1) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "ComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Act", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Sub Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Detail Description", bold: true },
                    { value: "Period", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true },
                    { value: "Actual Close Date Reviewer", bold: true }
                    //{ value: "Approver", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ActName },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ComSubTypeName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.Description },
                    { value: dataItem.ForMonth },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.RiskCategory },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated },
                    { value: dataItem.ReviewerDated }
                ]
            });

        }
        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 19; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
                //if (j == 15) {
                //    rows[i].cells[j].format = "MM/dd/yyyy";
                //}
                //if (j == 16) {
                //    rows[i].cells[j].format = "MM/dd/yyyy";
                //}
            }
        }

        excelExport(rows, ReportName);
    }

    //Event Based
    if ($("#dropdownlistComplianceType").val() == 1) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "ComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Act", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Sub Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Detail Description", bold: true },
                    { value: "Event Name", bold: true },
                    { value: "Event Nature", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true },
                    { value: "Actual Close Date Reviewer", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ActName },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ComSubTypeName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.Description },
                    { value: dataItem.EventName },
                    { value: dataItem.EventNature },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.RiskCategory },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated },
                    { value: dataItem.ReviewerDated }
                ]
            });

        }

        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 20; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
            }
        }

        excelExport(rows, ReportName);
    }

    //Event Based Checklist
    if ($("#dropdownlistComplianceType").val() == 4) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "ComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Act", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Sub Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Detail Description", bold: true },
                    { value: "Event Name", bold: true },
                    { value: "Event Nature", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ActName },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ComSubTypeName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.Description },
                    { value: dataItem.EventName },
                    { value: dataItem.EventNature },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.Risk },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated }
                ]
            });

        }

        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 19; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }

                if (rows[i].cells[15].value == 0) {
                    rows[i].cells[15].value = "High";
                }                 
                if (rows[i].cells[15].value == 1) {
                    rows[i].cells[15].value = "Medium";
                }                 
                if (rows[i].cells[15].value == 2) {
                    rows[i].cells[15].value = "Low";
                }
            }
        }

        excelExport(rows, ReportName);
    }

    //Statutory Checklist
    if ($("#dropdownlistComplianceType").val() == 2) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "ComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Act", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Detail Description", bold: true },
                    { value: "Period", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true },
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ActName },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.Description },
                    { value: dataItem.ForMonth },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.Risk },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated }
                ]
            });
        }


        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 17; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (rows[i].cells[13].value == 0) {
                    rows[i].cells[13].value = "High";
                }                 
                if (rows[i].cells[13].value == 1) {
                    rows[i].cells[13].value = "Medium";
                }                 
                if (rows[i].cells[13].value == 2) {
                    rows[i].cells[13].value = "Low";
                }

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
            }
        }
        excelExport(rows, ReportName);
    }

    //Internal
    if ($("#dropdownlistComplianceType").val() == 0) {
        debugger;
        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "InternalComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Period", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true },
                    { value: "Actual Close Date Reviewer", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.ForMonth },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.Risk },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated },
                    { value: dataItem.ReviewerDated }
                ]
            });

        }

        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 16; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (rows[i].cells[11].value == 0) {
                    rows[i].cells[11].value = "High";
                }                 
                if (rows[i].cells[11].value == 1) {
                    rows[i].cells[11].value = "Medium";
                }                 
                if (rows[i].cells[11].value == 2) {
                    rows[i].cells[11].value = "Low";
                }

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }

                //if (j == 12) {
                //    if (rows[i].cells[j].value != null) {
                //        rows[i].cells[j].format = "MM/dd/yyyy";
                //    }

                //}
                //if (j == 13) {
                //    if (rows[i].cells[j].value != null) {
                //        rows[i].cells[j].format = "MM/dd/yyyy";
                //    }

                //}
            }
        }

        excelExportInternal(rows, ReportName);
    }
    
    // Internal Checklist
    if ($("#dropdownlistComplianceType").val() == 3) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "InternalComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Period", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true }

                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.ForMonth },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.Risk },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated }
                ]
            });

        }

        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 15; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (rows[i].cells[11].value == 0) {
                    rows[i].cells[11].value = "High";
                }                 
                if (rows[i].cells[11].value == 1) {
                    rows[i].cells[11].value = "Medium";
                }                 
                if (rows[i].cells[11].value == 2) {
                    rows[i].cells[11].value = "Low";
                }

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
            }
        }

        excelExportInternal(rows, ReportName);
    }
    
    //return false;
    e.preventDefault();
}

function excelExport(rows, ReportName) {

    var workbook = new kendo.ooxml.Workbook({
        sheets: [
            {
                columns: [
                    { autoWidth: true },
                    { autoWidth: true },
                    { autoWidth: true },
                    { autoWidth: true },
                    { autoWidth: true },                    
                    { width: 300 },
                    { width: 180 },
                    { width: 300 },
                    { width: 300 },
                    { width: 300 },
                    { autoWidth: true },
                    { width: 100 },
                    { width: 100 },
                    { width: 150 },
                    { width: 100 },
                    { width: 150 },
                    { width: 150 },
                    { width: 180 },
                    { width: 180 },
                    { width: 180 }
                    //{ width: 150 },
                    //{ width: 100 },
                ],
                title: "CannedReport",
                rows: rows
            },
        ]
    });

    var nameOfPage = ReportName + "_CannedReport";
    //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });

}

function excelExportInternal(rows, ReportName) {

    var workbook = new kendo.ooxml.Workbook({
        sheets: [
            {
                columns: [
                    { autoWidth: true },
                    { autoWidth: true },
                    { autoWidth: true },
                    { autoWidth: true },
                    { autoWidth: true },                   
                    { width: 300 },
                    { width: 300 },
                    { width: 150 },
                    { width: 150 },
                    { width: 150 },
                    { autoWidth: true },
                    { width: 100 },
                    { width: 150 },
                    { width: 150 },
                    { width: 180 },
                    { width: 180 },
                    { width: 180 },
                    { width: 180 },
                    { width: 180 }
                ],
                title: "CannedReport",
                rows: rows
            },
        ]
    });

    var nameOfPage = ReportName + "_CannedReport";
    //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });

}

function exportReportAdvanced(e) {
    var customerName = document.getElementById('CustName').value;
    var ReportName = "";
    if ($("#dropdownlistComplianceType1").val() == -1) {
        ReportName = "Statutory";
    }
    if ($("#dropdownlistComplianceType1").val() == 0) {
        ReportName = "Internal";
    }
    if ($("#dropdownlistComplianceType1").val() == 1) {
        ReportName = "Event Based";
    }
    if ($("#dropdownlistComplianceType1").val() == 2) {
        ReportName = "Statutory Checklist";
    }
    if ($("#dropdownlistComplianceType1").val() == 3) {
        ReportName = "Internal Checklist";
    }
    if ($("#dropdownlistComplianceType1").val() == 4) {
        ReportName = "Event Based Checklist";
    }

    var ReportNameAdd = "Report of " + ReportName + " Compliances";

    var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    var grid = $("#grid1").getKendoGrid();

    //Statutory
    if ($("#dropdownlistComplianceType1").val() == -1) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "ComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Act", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Sub Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Detail Description", bold: true },
                    { value: "Period", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true },
                    { value: "Actual Close Date Reviewer", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ActName },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ComSubTypeName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.Description },
                    { value: dataItem.ForMonth },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.RiskCategory },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated },
                    { value: dataItem.ReviewerDated }
                ]
            });

        }
        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 19; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
            }
        }

        excelExport(rows, ReportName);
    }

    //Event Based
    if ($("#dropdownlistComplianceType1").val() == 1) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "ComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Act", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Sub Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Detail Description", bold: true },
                    { value: "Event Name", bold: true },
                    { value: "Event Nature", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true },
                    { value: "Actual Close Date Reviewer", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ActName },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ComSubTypeName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.Description },
                    { value: dataItem.EventName },
                    { value: dataItem.EventNature },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.RiskCategory },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated },
                    { value: dataItem.ReviewerDated }
                ]
            });

        }

        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 20; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
            }
        }

        excelExport(rows, ReportName);
    }

    //Event Based Checklist
    if ($("#dropdownlistComplianceType1").val() == 4) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "ComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Act", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Sub Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Detail Description", bold: true },
                    { value: "Event Name", bold: true },
                    { value: "Event Nature", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ActName },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ComSubTypeName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.Description },
                    { value: dataItem.EventName },
                    { value: dataItem.EventNature },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.Risk },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated }
                ]
            });

        }

        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 19; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }

                if (rows[i].cells[15].value == 0) {
                    rows[i].cells[15].value = "High";
                }                 
                if (rows[i].cells[15].value == 1) {
                    rows[i].cells[15].value = "Medium";
                }                 
                if (rows[i].cells[15].value == 2) {
                    rows[i].cells[15].value = "Low";
                }
            }
        }

        excelExport(rows, ReportName);
    }

    //Statutory Checklist
    if ($("#dropdownlistComplianceType1").val() == 2) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "ComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Act", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Detail Description", bold: true },
                    { value: "Period", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true },
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ActName },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.Description },
                    { value: dataItem.ForMonth },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.Risk },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated }
                ]
            });
        }


        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 17; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (rows[i].cells[13].value == 0) {
                    rows[i].cells[13].value = "High";
                }                 
                if (rows[i].cells[13].value == 1) {
                    rows[i].cells[13].value = "Medium";
                }                 
                if (rows[i].cells[13].value == 2) {
                    rows[i].cells[13].value = "Low";
                }

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
            }
        }
        excelExport(rows, ReportName);
    }

    //Internal
    if ($("#dropdownlistComplianceType1").val() == 0) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "InternalComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Period", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true },
                    { value: "Actual Close Date Reviewer", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.ForMonth },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.Risk },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated },
                    { value: dataItem.ReviewerDated }
                ]
            });

        }

        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 16; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (rows[i].cells[11].value == 0) {
                    rows[i].cells[11].value = "High";
                }                 
                if (rows[i].cells[11].value == 1) {
                    rows[i].cells[11].value = "Medium";
                }                 
                if (rows[i].cells[11].value == 2) {
                    rows[i].cells[11].value = "Low";
                }

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
            }
        }
        excelExportInternal(rows, ReportName);
    }

    //Internal Checklist
    if ($("#dropdownlistComplianceType1").val() == 3) {

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportNameAdd }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {
                cells: [
                    { value: "InternalComplianceID", bold: true },
                    { value: "Location", bold: true },
                    { value: "State", bold: true },
                    { value: "Region", bold: true },
                    { value: "Zone", bold: true },
                    { value: "Category Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Period", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "Close Date", bold: true },
                    { value: "Status", bold: true },
                    { value: "Risk", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Actual Close Date Performer", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [ // dataItem."Whatever Your Attributes Are"
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.state },
                    { value: dataItem.region },
                    { value: dataItem.zone },
                    { value: dataItem.ComCategoryName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.ForMonth },
                    { value: dataItem.ScheduledOn },
                    { value: dataItem.CloseDate },
                    { value: dataItem.Status },
                    { value: dataItem.Risk },
                    { value: dataItem.PerformerName },
                    { value: dataItem.ReviewerName },
                    { value: dataItem.PerformerDated }
                ]
            });

        }

        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 15; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;

                if (rows[i].cells[11].value == 0) {
                    rows[i].cells[11].value = "High";
                }                 
                if (rows[i].cells[11].value == 1) {
                    rows[i].cells[11].value = "Medium";
                }                 
                if (rows[i].cells[11].value == 2) {
                    rows[i].cells[11].value = "Low";
                }

                if (i == 4) {
                    rows[4].cells[j].background = "#A9A9A9";
                }
            }
        }
        excelExportInternal(rows, ReportName);
    }

    e.preventDefault();
};