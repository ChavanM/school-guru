﻿function exportReport(e) {
  
    var ReportName = Report of Internal Compliances;
    var customerName = document.getElementById('CustName').value;
    var todayDate = new Date().toJSON().slice(0, 10).replace(-g, '');
    var grid = $(#grid).getKendoGrid();

    var rows = [
        {
            cells [
                { value Entity Location, bold true },
                { value customerName }
            ]
        },
        {
            cells [
                { value Report Name, bold true },
                { value ReportName }
            ]
        },
        {
            cells [
                { value Report Generated On, bold true },
                { value todayDate }
            ]
        },
        {
            cells [
                { value  }
            ]
        },
        {
            cells [
                { value Location, bold true },
                { value Act Name, bold true },
                { value Short Description, bold true },
                { value Performer, bold true },
                { value Reviewer, bold true },
                { value Due Date, bold true },
                { value For Month, bold true },
                { value Status, bold true },
                { value Risk Category, bold true }
            ]
        }
    ];

    var trs = grid.dataSource;
    var filteredDataSource = new kendo.data.DataSource({
        data trs.data(),
        filter trs.filter()
    });

    filteredDataSource.read();
    var data = filteredDataSource.view();
    for (var i = 0; i  data.length; i++) {
        var dataItem = data[i];
        rows.push({
            cells [
                { value dataItem.Branch },
                { value dataItem.Name },
                { value dataItem.ShortDescription },
                { value dataItem.Performer },
                { value dataItem.Reviewer },
                { value dataItem.ScheduledOn },
                { value dataItem.ForMonth },
                { value dataItem.Status },
                { value dataItem.RiskCategory },
            ]
        });
    }
    for (var i = 4; i  rows.length; i++) {
        for (var j = 0; j  9; j++) {
            rows[i].cells[j].borderBottom = #000000;
            rows[i].cells[j].borderLeft = #000000;
            rows[i].cells[j].borderRight = #000000;
            rows[i].cells[j].borderTop = #000000;
            rows[i].cells[j].hAlign = left;
            rows[i].cells[j].vAlign = top;
            rows[i].cells[j].wrap = true;
        }
    }
    excelExport(rows, ReportName);
    e.preventDefault();
}

function excelExport(rows, ReportName) {

    var workbook = new kendo.ooxml.Workbook({
        sheets [
            {
                columns [
                    { width 180 },
                    { width 300 },
                    { width 300 },
                    { width 180 },
                    { width 180 },
                    { width 100 },
                    { width 100 },
                    { width 150 },
                    { width 100 }
                ],
                title Report,
                rows rows
            },
        ]
    });

    var nameOfPage = InternalComplianceReport;
    kendo.saveAs({ dataURI workbook.toDataURL(), fileName nameOfPage +  .xlsx });

}


function ClearAllFilterMain(e) {
    $(#dropdowntree).data(kendoDropDownTree).value([]);
    $(#dropdownlistRisk).data(kendoDropDownTree).value([]);
    $(#dropdownlistStatus).data(kendoDropDownTree).value([]);
    $(#dropdownUser).data(kendoDropDownTree).value([]);
    $('#ClearfilterMain').css('display', 'none');
    $(#grid).data(kendoGrid).dataSource.filter({});
    e.preventDefault();
}

function fcloseStory(obj) {

    var DataId = $(obj).attr('data-Id');
    var dataKId = $(obj).attr('data-K-Id');
    var seq = $(obj).attr('data-seq');
    var deepspan = $('#' + DataId + '  li  span  span.k-i-close')[seq];
    $(deepspan).trigger('click');
    var upperli = $('#' + dataKId);
    $(upperli).remove();

    for rebind if any pending filter is present (Main Grid)
    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
    CheckFilterClearorNotMain();
};

function CheckFilterClearorNotMain() {
    if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)
        ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
        $('#ClearfilterMain').css('display', 'none');
    }
}


