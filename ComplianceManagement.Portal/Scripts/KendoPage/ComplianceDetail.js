﻿
function ClearAllFilterMain(e) {
 
    if ($("#dropdownACT").val() != undefined && $("#dropdownACT").val() != "") {
        $("#dropdownACT").data("kendoDropDownList").value([]);
    }
    $("#dropdownType").data("kendoDropDownList").value([]);


    $("#dropdowntree").data("kendoDropDownTree").value([]);
    //$("#dropdownlistUserRole").data("kendoDropDownList").value([]);
    $("#dropdownfunction").data("kendoDropDownList").select(0);
    $("#dropdownACT").data("kendoDropDownList").select(0);
    $("#dropdownFrequency").data("kendoDropDownList").select(0);
    $('#ClearfilterMain').css('display', 'none');
    $("#grid").data("kendoGrid").dataSource.filter({});
    BindGrid();
    e.preventDefault();
}

function fcloseStory(obj) {
    var DataId = $(obj).attr('data-Id');
    var dataKId = $(obj).attr('data-K-Id');
    var seq = $(obj).attr('data-seq');
    var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
    $(deepspan).trigger('click');
    var upperli = $('#' + dataKId);
    $(upperli).remove();

    //for rebind if any pending filter is present (Main Grid)
    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
    //fCreateStoryBoard('dropdownlistUserRole', 'filterrole', 'role');
    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
    fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
    fCreateStoryBoard('dropdownFrequency', 'filterCompType', 'frequency');


    CheckFilterClearorNotMain();
};

function CheckFilterClearorNotMain() {
    if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistUserRole').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
        //$('#ClearfilterMain').css('display', 'none');
    }
}

function fCreateStoryBoard(Id, div, filtername) {

    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
    $('#' + div).html('');
    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
    $('#' + div).css('display', 'block');

    if (div == 'filtersstoryboard') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
        //$('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filtertype') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
    }
    else if (div == 'filterrole') {
        $('#' + div).append('Role&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        //$('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterstatus') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        //$('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterpstData1') {
        $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCategory') {
        $('#' + div).append('Category&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterAct') {
        $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompSubType') {
        $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompType') {
        $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtersstoryboard1') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtertype1') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterrisk1') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterFY') {
        $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterUser') {
        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterstatus1') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }

    for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
        $(button).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
        var buttontest = $($(button).find('span')[0]).text();
        if (buttontest.length > 10) {
            buttontest = buttontest.substring(0, 10).concat("...");
        }
        $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:1px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
        //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
    }

    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
        $('#' + div).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

    }
    CheckFilterClearorNotMain();
}

function FilterGrid() {



    var IsDept = document.getElementById('IsDeptId').value;

    var IsSI = document.getElementById('IsSIId').value;
    //alert(IsDept);

    //location details
    //var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
    //var locationsdetails = [];
    //$.each(list1, function (i, v) {
    //    locationsdetails.push({
    //        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
    //    });
    //});
    var locationsdetails = [];
    if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
        locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
    }


    //Role Details
    var Roledetails = [];
    if ($("#dropdownlistUserRole").val() != "-1") {
        Roledetails.push({
            field: "RoleID", operator: "eq", value: parseInt($("#dropdownlistUserRole").val())
        });
    }

    //Frequency Details
    var Frequencydetails = [];
    if ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined) {
        Frequencydetails.push({
            field: "Frequency", operator: "eq", value: parseInt($("#dropdownFrequency").val())
        });
    }

    //Act Details
    var Actdetails = [];
    
    //if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
    //    Actdetails.push({
    //        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
    //    });
    //}

    //Function Details
    //var Functiondetails = [];

    //if (IsDept == 1) {
    //    if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {            
    //        Functiondetails.push({
    //            field: "DepartmentID", operator: "eq", value: parseInt($("#dropdownfunction").val())
    //        });
    //    }
    //}
    //else {    
    //    if (IsSI != -1) {
    //        if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
    //            Functiondetails.push({
    //                field: "InternalComplianceCategoryID", operator: "eq", value: parseInt($("#dropdownfunction").val())
    //            });
    //        }
    //    }
    //    else
    //    {
    //        if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
    //            Functiondetails.push({
    //                field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
    //            });
    //        }
    //    }
       
    //}
    debugger;

    var dataSource = $("#grid").data("kendoGrid").dataSource;

    //Five
    //if (locationsdetails.length > 0
    //    && Roledetails.length > 0
    //    && Functiondetails.length > 0
    //    && Actdetails.length > 0
    //    && Frequencydetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}

    ////Four
    //else if (locationsdetails.length > 0
    //    && Roledetails.length > 0
    //    && Functiondetails.length > 0
    //    && Frequencydetails.length>0 ) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}

    //else if (locationsdetails.length > 0
    //    && Functiondetails.length > 0
    //    && Actdetails.length > 0
    //    && Frequencydetails.length > 0 ) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}

    //else if (Roledetails.length > 0
    //    && Functiondetails.length > 0
    //    && Actdetails.length > 0
    //    && Frequencydetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}

    //else if (Roledetails.length > 0
    //&& locationsdetails.length > 0
    //&& Actdetails.length > 0
    //&& Frequencydetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            },
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}
    //else if (Roledetails.length > 0
    //    && locationsdetails.length > 0
    //    && Actdetails.length > 0
    //    && Functiondetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            },
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            }
    //        ]
    //    });
    //}

    ////Three
    //else if (locationsdetails.length > 0
    //    && Roledetails.length > 0
    //    && Functiondetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            }
    //            ,
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            }
    //        ]
    //    });
    //}

    //else if (Roledetails.length > 0
    //    && Functiondetails.length > 0
    //    && Actdetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            }
    //        ]
    //    });
    //}

    //else if (Functiondetails.length > 0
    //    && Actdetails.length > 0
    //    && Frequencydetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}

    //else if (Actdetails.length > 0
    //    && Frequencydetails.length > 0
    //    && locationsdetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            },
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            }
    //        ]
    //    });
    //}


    //    //Two
    //else if (Roledetails.length > 0
    //    && Actdetails.length > 0
    //    && Frequencydetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}

    //else if (Functiondetails.length > 0
    //    && Actdetails.length > 0
    //    && Frequencydetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}

    ////one
    //else if (Functiondetails.length > 0
    //    && Frequencydetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Functiondetails
    //            },
    //            {
    //                logic: "or",
    //                filters: Frequencydetails
    //            }
    //        ]
    //    });
    //}

    //else if (Actdetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Actdetails
    //            }
    //        ]
    //    });
    //}

    //else if (locationsdetails.length > 0) {

    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: locationsdetails
    //            }
    //        ]
    //    });
    //}

    //else if (Roledetails.length > 0) {
    //    dataSource.filter({
    //        logic: "and",
    //        filters: [
    //            {
    //                logic: "or",
    //                filters: Roledetails
    //            }
    //        ]
    //    });
    //}
    var finalSelectedfilter = { logic: "and", filters: [] };

    if (locationsdetails.length > 0
                    || Roledetails.length > 0
                    //|| Functiondetails.length > 0
                    || Actdetails.length > 0
                    || Frequencydetails.length > 0
                    || ($("#dropdownlistUserRole").val() != $("#dropdownlistUserRole").val() != "Role" && $("#dropdownlistUserRole").val() != null && $("#dropdownlistUserRole").val() != undefined)
                    || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                    || ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined)
                    || ($("#dropdownfunction").val() != undefined && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != "")
                    || ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined)

                    || datedetails.length > 0) {

        if (locationsdetails.length > 0) {
            var LocationFilter = { logic: "or", filters: [] };

            $.each(locationsdetails, function (i, v) {
                LocationFilter.filters.push({
                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(LocationFilter);
        }
        if ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined) {
            var TypeFilter = { logic: "or", filters: [] };
            TypeFilter.filters.push({
                field: "InternalComplianceTypeName", operator: "eq", value: $("#dropdownType").val()
            });
            finalSelectedfilter.filters.push(TypeFilter);
        }

        if ($("#dropdownlistUserRole").val() != undefined && $("#dropdownlistUserRole").val() != null && $("#dropdownlistUserRole").val() != "-1" && $("#dropdownlistUserRole").val() != "") {
            var SeqFilter = { logic: "or", filters: [] };
            SeqFilter.filters.push({
                field: "RoleID", operator: "eq", value: $("#dropdownlistUserRole").val()
            });
            finalSelectedfilter.filters.push(SeqFilter);
        }

        if ($("#dropdownfunction").val() != undefined && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != "-1" && $("#dropdownfunction").val() != "") {
            var SeqFilter = { logic: "or", filters: [] };

            if (IsDept == 1) {
                if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
                    Functiondetails.push({
                        field: "DepartmentID", operator: "eq", value: parseInt($("#dropdownfunction").val())
                    });
                }
            }
            else {
                if (IsSI == 0) {
                    SeqFilter.filters.push({
                        field: "InternalComplianceCategoryID", operator: "eq", value: $("#dropdownfunction").val()
                    });
                }
                else {
                    SeqFilter.filters.push({
                        field: "ComplianceCategoryId", operator: "eq", value: $("#dropdownfunction").val()
                    });
                }
            }
            finalSelectedfilter.filters.push(SeqFilter);
        }
        if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "-1" && $("#dropdownACT").val() != undefined) {
            var ActFilter = { logic: "or", filters: [] };

            ActFilter.filters.push({
                field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
            });

            finalSelectedfilter.filters.push(ActFilter);
        }

        if ($("#dropdownFrequency").val() != undefined && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != "-1" && $("#dropdownFrequency").val() != "") {
            var SeqFilter = { logic: "or", filters: [] };
            SeqFilter.filters.push({
                field: "Frequency", operator: "eq", value: $("#dropdownFrequency").val()
            });
            finalSelectedfilter.filters.push(SeqFilter);
        }


        if (finalSelectedfilter.filters.length > 0) {
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.filter(finalSelectedfilter);
        }
        else {
            $("#grid").data("kendoGrid").dataSource.filter({});
        }
    }
    else {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }
}
