﻿
function ClearAllFilterMain(e) { 
    $("#dropdowntree").data("kendoDropDownTree").value([]);    
    $('#ClearfilterMain').css('display', 'none');
    $("#grid").data("kendoGrid").dataSource.filter({});
    e.preventDefault();
}

function fcloseStory(obj) {
    var DataId = $(obj).attr('data-Id');
    var dataKId = $(obj).attr('data-K-Id');
    var seq = $(obj).attr('data-seq');
    var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
    $(deepspan).trigger('click');
    var upperli = $('#' + dataKId);
    $(upperli).remove();

    //for rebind if any pending filter is present (Main Grid)
    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
    fCreateStoryBoard('dropdownlistUserRole', 'filterrole', 'role');    
    CheckFilterClearorNotMain();
};

function CheckFilterClearorNotMain() {
    if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdowntreecustomer').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistUserRole').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlisRRC').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)
        ($($($('#dropdownlistUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
        $('#ClearfilterMain').css('display', 'none');
    }
}

function fCreateStoryBoard(Id, div, filtername) {

    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
    $('#' + div).html('');
    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
    $('#' + div).css('display', 'block');

    if (div == 'filtersstoryboard') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filtertype') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
    }
    else if (div == 'filterrole') {
        $('#' + div).append('Role&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterstatus') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterpstData1') {
        $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCategory') {
        $('#' + div).append('Category&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterAct') {
        $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompSubType') {
        $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompType') {
        $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtersstoryboard1') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtertype1') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterrisk1') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterFY') {
        $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterUser') {
        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterstatus1') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }

    for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
        $(button).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
        var buttontest = $($(button).find('span')[0]).text();
        if (buttontest.length > 10) {
            buttontest = buttontest.substring(0, 10).concat("...");
        }
        $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
    }

    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
        $('#' + div).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

    }
    CheckFilterClearorNotMain();
}

function FilterGrid() {

    //Customer details   
    var Customerdetails = [];
    if ($("#dropdowntreecustomer").val() != "" && $("#dropdowntreecustomer").val() != null && $("#dropdowntreecustomer").val() != undefined) {
        Customerdetails.push({
            field: "CustomerID", operator: "eq", value: parseInt($("#dropdowntreecustomer").val())
        });
    }

    //location details
    var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
    var locationsdetails = [];
    $.each(list1, function (i, v) {
        locationsdetails.push({
            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
        });
    });
  
    //Role Details
    var Roledetails = [];
    if ($("#dropdownlistUserRole").val() != "-1") {
        Roledetails.push({
            field: "RoleID", operator: "eq", value: parseInt($("#dropdownlistUserRole").val())
        });
    }

    //Registers/Returns/Challans Details
    var RRCdetails = [];
    if ($("#dropdownlisRRC").val() != "-1") {
        RRCdetails.push({
            field: "RoleID", operator: "eq", value: parseInt($("#dropdownlisRRC").val())
        });
    }
    
    //User Details
    var Userdetails = [];
    if ($("#dropdownlistUser").val() != "" && $("#dropdownlistUser").val() != null && $("#dropdownlistUser").val() != undefined) {
        Userdetails.push({
            field: "UserID", operator: "eq", value: parseInt($("#dropdownlistUser").val())
        });
    }

    var dataSource = $("#grid").data("kendoGrid").dataSource;
    //five
    if (Customerdetails.length>0
        && locationsdetails.length > 0
        && Roledetails.length > 0     
        && RRCdetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                 {
                     logic: "or",
                     filters: Customerdetails
                 },
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },               
                {
                    logic: "or",
                    filters: RRCdetails
                },
                 {
                     logic: "or",
                     filters: Userdetails
                 }
            ]
        });
    }

    //four
    else if (locationsdetails.length > 0
        && Roledetails.length > 0
        && RRCdetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }

    else if (Customerdetails.length > 0
        && locationsdetails.length > 0
        && Roledetails.length > 0
        && RRCdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                 {
                     logic: "or",
                     filters: Customerdetails
                 },
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                }
            ]
        });
    }

    else if (Customerdetails.length > 0
        && locationsdetails.length > 0
        && Roledetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }
    else if (Customerdetails.length > 0
          && Roledetails.length > 0
          && RRCdetails.length > 0
          && Userdetails.length > 0) {
          dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                },
                 {
                     logic: "or",
                     filters: Userdetails
                 }
            ]
        });
    }
        //three       
    else if (Customerdetails.length > 0
        && locationsdetails.length > 0
        && Roledetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                }
            ]
        });
    }      
    else if (Customerdetails.length > 0
          && locationsdetails.length > 0
          && RRCdetails.length > 0) {
          dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                }
            ]
        });
    }
    else if (Customerdetails.length > 0
        && locationsdetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }                        
    else if (Customerdetails.length > 0
        && Roledetails.length > 0
        && RRCdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                }
            ]
        });
    }          
    else if (Customerdetails.length > 0
        && RRCdetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }                
    else if (Customerdetails.length > 0
        && Roledetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }                
    else if (locationsdetails.length > 0
        && Roledetails.length > 0
        && RRCdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                }
            ]
        });
    }                
    else if (locationsdetails.length > 0
        && Roledetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }        
    else if (Roledetails.length > 0
        && RRCdetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }        
      
        //two
    else if (Customerdetails.length > 0
        && locationsdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }        
    else if (Customerdetails.length > 0
        && Roledetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                }
            ]
        });
    }             
    else if (Customerdetails.length > 0
        && RRCdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                }
            ]
        });
    }              
    else if (Customerdetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }          
    else if (locationsdetails.length > 0
        && Roledetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Roledetails
                }
            ]
        });
    }                
    else if (locationsdetails.length > 0
        && RRCdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                }
            ]
        });
    }        
        
    else if (locationsdetails.length > 0
          && Userdetails.length > 0) {
          dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }                
    else if (Roledetails.length > 0
        && RRCdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: RRCdetails
                }
            ]
        });
    }
        
    else if (Roledetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Roledetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }
    else if (RRCdetails.length > 0
        && Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: RRCdetails
                },
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }      
        //one
    else if (Customerdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Customerdetails
                }
            ]
        });
    }

    else if (Userdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Userdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }

    else if (Roledetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Roledetails
                }
            ]
        });
    }
    else if (RRCdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: RRCdetails
                }
            ]
        });
    }
    else {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }
}
